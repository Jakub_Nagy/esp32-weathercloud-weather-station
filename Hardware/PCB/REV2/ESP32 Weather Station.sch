<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="yes" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="11" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="PM_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="PF_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="WFL_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="no" active="no"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="no" active="no"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="no" active="no"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="no" active="no"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-02" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-15" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="DrillLegend_02-15" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="160" name="O_Dim" color="12" fill="11" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="no" active="no"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="no" active="no"/>
<layer number="193" name="mPins" color="7" fill="1" visible="no" active="no"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="no" active="no"/>
<layer number="195" name="mNames" color="7" fill="1" visible="no" active="no"/>
<layer number="196" name="mValues" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ESP32-DEVKITV1">
<packages>
<package name="ESP32-DEVKITV1">
<pad name="1" x="-22.87" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="2" x="-20.33" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="3" x="-17.79" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="4" x="-15.25" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="5" x="-12.71" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="6" x="-10.17" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="7" x="-7.63" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="8" x="-5.09" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="9" x="-2.55" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="10" x="-0.01" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="11" x="2.53" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="12" x="5.07" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="13" x="7.61" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="14" x="10.15" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="15" x="12.69" y="-13.54" drill="1" diameter="1.9304"/>
<pad name="30" x="-22.87" y="11.23" drill="1" diameter="1.9304"/>
<pad name="29" x="-20.33" y="11.23" drill="1" diameter="1.9304"/>
<pad name="28" x="-17.79" y="11.23" drill="1" diameter="1.9304"/>
<pad name="27" x="-15.25" y="11.23" drill="1" diameter="1.9304"/>
<pad name="26" x="-12.71" y="11.23" drill="1" diameter="1.9304"/>
<pad name="25" x="-10.17" y="11.23" drill="1" diameter="1.9304"/>
<pad name="24" x="-7.63" y="11.23" drill="1" diameter="1.9304"/>
<pad name="23" x="-5.09" y="11.23" drill="1" diameter="1.9304"/>
<pad name="22" x="-2.55" y="11.23" drill="1" diameter="1.9304"/>
<pad name="21" x="-0.01" y="11.23" drill="1" diameter="1.9304"/>
<pad name="20" x="2.53" y="11.23" drill="1" diameter="1.9304"/>
<pad name="19" x="5.07" y="11.23" drill="1" diameter="1.9304"/>
<pad name="18" x="7.61" y="11.23" drill="1" diameter="1.9304"/>
<pad name="17" x="10.15" y="11.23" drill="1" diameter="1.9304"/>
<pad name="16" x="12.69" y="11.23" drill="1" diameter="1.9304"/>
<text x="-22.21" y="-11.2" size="1.016" layer="21" rot="R90">3V3</text>
<text x="-19.67" y="-11.2" size="1.016" layer="21" rot="R90">GND</text>
<text x="-17.13" y="-11.2" size="1.016" layer="21" rot="R90">IO15</text>
<text x="-14.59" y="-11.2" size="1.016" layer="21" rot="R90">IO2</text>
<text x="-12.05" y="-11.2" size="1.016" layer="21" rot="R90">IO4</text>
<text x="-9.51" y="-11.2" size="1.016" layer="21" rot="R90">IO16</text>
<text x="-6.97" y="-11.2" size="1.016" layer="21" rot="R90">IO17</text>
<text x="-4.43" y="-11.2" size="1.016" layer="21" rot="R90">IO5</text>
<text x="-1.89" y="-11.2" size="1.016" layer="21" rot="R90">IO18</text>
<text x="0.65" y="-11.2" size="1.016" layer="21" rot="R90">IO19</text>
<text x="3.19" y="-11.2" size="1.016" layer="21" rot="R90">IO21</text>
<text x="5.73" y="-11.2" size="1.016" layer="21" rot="R90">IO3</text>
<text x="8.27" y="-11.2" size="1.016" layer="21" rot="R90">IO1</text>
<text x="10.81" y="-11.2" size="1.016" layer="21" rot="R90">IO22</text>
<text x="13.35" y="-11.2" size="1.016" layer="21" rot="R90">IO23</text>
<text x="-22.19" y="6.52" size="1.016" layer="21" rot="R90">VIN</text>
<text x="-19.65" y="6.52" size="1.016" layer="21" rot="R90">GND</text>
<text x="-17.11" y="6.52" size="1.016" layer="21" rot="R90">IO13</text>
<text x="-14.57" y="6.52" size="1.016" layer="21" rot="R90">IO12</text>
<text x="-12.03" y="6.52" size="1.016" layer="21" rot="R90">IO14</text>
<text x="-9.49" y="6.52" size="1.016" layer="21" rot="R90">IO27</text>
<text x="-6.95" y="6.52" size="1.016" layer="21" rot="R90">IO26</text>
<text x="-4.41" y="6.52" size="1.016" layer="21" rot="R90">IO25</text>
<text x="-1.87" y="6.52" size="1.016" layer="21" rot="R90">IO33</text>
<text x="0.67" y="6.52" size="1.016" layer="21" rot="R90">IO32</text>
<text x="3.21" y="6.52" size="1.016" layer="21" rot="R90">IO35</text>
<text x="5.75" y="6.52" size="1.016" layer="21" rot="R90">IO34</text>
<text x="8.29" y="6.52" size="1.016" layer="21" rot="R90">VN</text>
<text x="10.83" y="6.52" size="1.016" layer="21" rot="R90">VP</text>
<text x="13.37" y="6.52" size="1.016" layer="21" rot="R90">EN</text>
<text x="-4.93" y="-3.18" size="1.9304" layer="21">ESP32-Devkit V1</text>
<wire x1="-33" y1="12.7" x2="19" y2="12.7" width="0.254" layer="21"/>
<wire x1="19" y1="12.7" x2="19" y2="-15.2" width="0.254" layer="21"/>
<wire x1="19" y1="-15.2" x2="-33" y2="-15.2" width="0.254" layer="21"/>
<wire x1="-33" y1="-15.2" x2="-33" y2="12.7" width="0.254" layer="21"/>
<text x="-24.13" y="13.97" size="1.27" layer="21">&gt;NAME</text>
<text x="5" y="-17.24" size="1.27" layer="27">ESP32-DEVKITV1</text>
</package>
</packages>
<symbols>
<symbol name="ESP32-DEVKITV1">
<wire x1="-25.4" y1="-12.7" x2="-25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="12.7" x2="16" y2="12.7" width="0.254" layer="94"/>
<wire x1="16" y1="12.7" x2="16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="16" y1="-12.7" x2="-25.4" y2="-12.7" width="0.254" layer="94"/>
<pin name="3V3" x="-22.86" y="-17.78" length="middle" rot="R90"/>
<pin name="GND" x="-20.32" y="-17.78" length="middle" rot="R90"/>
<pin name="IO15" x="-17.78" y="-17.78" length="middle" rot="R90"/>
<pin name="IO2" x="-15.24" y="-17.78" length="middle" rot="R90"/>
<pin name="IO4" x="-12.7" y="-17.78" length="middle" rot="R90"/>
<pin name="IO16" x="-10.16" y="-17.78" length="middle" rot="R90"/>
<pin name="IO17" x="-7.62" y="-17.78" length="middle" rot="R90"/>
<pin name="IO5" x="-5.08" y="-17.78" length="middle" rot="R90"/>
<pin name="IO18" x="-2.54" y="-17.78" length="middle" rot="R90"/>
<pin name="IO19" x="0" y="-17.78" length="middle" rot="R90"/>
<pin name="IO21" x="2.54" y="-17.78" length="middle" rot="R90"/>
<pin name="IO3" x="5.08" y="-17.78" length="middle" rot="R90"/>
<pin name="IO1" x="7.62" y="-17.78" length="middle" rot="R90"/>
<pin name="IO22" x="10.16" y="-17.78" length="middle" rot="R90"/>
<pin name="IO23" x="12.7" y="-17.78" length="middle" rot="R90"/>
<pin name="EN" x="12.7" y="17.78" length="middle" rot="R270"/>
<pin name="VP" x="10.16" y="17.78" length="middle" rot="R270"/>
<pin name="VN" x="7.62" y="17.78" length="middle" rot="R270"/>
<pin name="IO34" x="5.08" y="17.78" length="middle" rot="R270"/>
<pin name="IO35" x="2.54" y="17.78" length="middle" rot="R270"/>
<pin name="IO32" x="0" y="17.78" length="middle" rot="R270"/>
<pin name="IO33" x="-2.54" y="17.78" length="middle" rot="R270"/>
<pin name="IO25" x="-5.08" y="17.78" length="middle" rot="R270"/>
<pin name="IO26" x="-7.62" y="17.78" length="middle" rot="R270"/>
<pin name="IO27" x="-10.16" y="17.78" length="middle" rot="R270"/>
<pin name="IO14" x="-12.7" y="17.78" length="middle" rot="R270"/>
<pin name="IO12" x="-15.24" y="17.78" length="middle" rot="R270"/>
<pin name="IO13" x="-17.78" y="17.78" length="middle" rot="R270"/>
<pin name="GND1" x="-20.32" y="17.78" length="middle" rot="R270"/>
<pin name="VIN" x="-22.86" y="17.78" length="middle" rot="R270"/>
<text x="-26.67" y="5.08" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<text x="18.4" y="-12.7" size="1.27" layer="96" rot="R90">ESP32-DEVKITV1</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP32DEVKITV1">
<gates>
<gate name="G$1" symbol="ESP32-DEVKITV1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP32-DEVKITV1">
<connects>
<connect gate="G$1" pin="3V3" pad="1"/>
<connect gate="G$1" pin="EN" pad="16"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GND1" pad="29"/>
<connect gate="G$1" pin="IO1" pad="13"/>
<connect gate="G$1" pin="IO12" pad="27"/>
<connect gate="G$1" pin="IO13" pad="28"/>
<connect gate="G$1" pin="IO14" pad="26"/>
<connect gate="G$1" pin="IO15" pad="3"/>
<connect gate="G$1" pin="IO16" pad="6"/>
<connect gate="G$1" pin="IO17" pad="7"/>
<connect gate="G$1" pin="IO18" pad="9"/>
<connect gate="G$1" pin="IO19" pad="10"/>
<connect gate="G$1" pin="IO2" pad="4"/>
<connect gate="G$1" pin="IO21" pad="11"/>
<connect gate="G$1" pin="IO22" pad="14"/>
<connect gate="G$1" pin="IO23" pad="15"/>
<connect gate="G$1" pin="IO25" pad="23"/>
<connect gate="G$1" pin="IO26" pad="24"/>
<connect gate="G$1" pin="IO27" pad="25"/>
<connect gate="G$1" pin="IO3" pad="12"/>
<connect gate="G$1" pin="IO32" pad="21"/>
<connect gate="G$1" pin="IO33" pad="22"/>
<connect gate="G$1" pin="IO34" pad="19"/>
<connect gate="G$1" pin="IO35" pad="20"/>
<connect gate="G$1" pin="IO4" pad="5"/>
<connect gate="G$1" pin="IO5" pad="8"/>
<connect gate="G$1" pin="VIN" pad="30"/>
<connect gate="G$1" pin="VN" pad="18"/>
<connect gate="G$1" pin="VP" pad="17"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diy-modules">
<description>&lt;b&gt;DIY Modules for Arduino, Raspberry Pi, CubieBoard etc.&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
The library contains a list of symbols and footprints for popular, cheap and easy-to-use electronic modules.&lt;br&gt;
The modules are intend to work with microprocessor-based platforms such as &lt;a href="http://arduino.cc"&gt;Arduino&lt;/a&gt;, &lt;a href="http://raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt;, &lt;a href="http://cubieboard.org/"&gt;CubieBoard&lt;/a&gt;, &lt;a href="http://beagleboard.org/"&gt;BeagleBone&lt;/a&gt; and many others. There are many manufacturers of the modules in the world. Almost all of them can be bought on &lt;a href="ebay.com"&gt;ebay.com&lt;/a&gt;.&lt;br&gt;
&lt;br&gt;
By using this library, you can design a PCB for devices created with usage of modules. Even if you do not need to create PCB design, you can also use the library to quickly document your work by drawing schematics of devices built by you.&lt;br&gt;
&lt;br&gt;
The latest version, examples, photos and much more can be found at: &lt;b&gt;&lt;a href="http://diymodules.org/eagle"&gt;diymodules.org/eagle&lt;/a&gt;&lt;/b&gt;&lt;br&gt;&lt;br&gt;
Comments, suggestions and bug reports please send to: &lt;b&gt;&lt;a href="mailto:eagle@diymodules.org"&gt;eagle@diymodules.org&lt;/b&gt;&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Version: 1.8.0 (2017-Jul-02)&lt;/i&gt;&lt;br&gt;
&lt;i&gt;Created by: Miroslaw Brudnowski&lt;/i&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Released under the Creative Commons Attribution 4.0 International License: &lt;a href="http://creativecommons.org/licenses/by/4.0"&gt;http://creativecommons.org/licenses/by/4.0&lt;/a&gt;&lt;/i&gt;
&lt;br&gt;&lt;br&gt;
&lt;center&gt;
&lt;a href="http://diymodules.org/eagle"&gt;&lt;img src="http://www.diymodules.org/img/diymodules-lbr-image.php?v=1.8.0" alt="DIYmodules.org"&gt;&lt;/a&gt;
&lt;/center&gt;</description>
<packages>
<package name="LIGHT-SENSOR-BH1750">
<description>&lt;b&gt;Digital Light Sensor&lt;/b&gt; based on &lt;b&gt;BH1750&lt;/b&gt;</description>
<wire x1="-8.255" y1="6.35" x2="-6.985" y2="6.35" width="0.127" layer="21"/>
<text x="0" y="8.255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="1" x="-7.62" y="-5.08" drill="1" shape="square"/>
<pad name="2" x="-7.62" y="-2.54" drill="1"/>
<pad name="3" x="-7.62" y="0" drill="1"/>
<pad name="4" x="-7.62" y="2.54" drill="1"/>
<wire x1="-6.985" y1="6.35" x2="-6.35" y2="5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="5.715" x2="-6.35" y2="4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-6.985" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.985" y1="3.81" x2="-6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.985" y2="1.27" width="0.127" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-6.35" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.985" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-8.255" y2="6.35" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.255" y2="3.81" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.81" x2="-8.89" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.905" x2="-8.255" y2="1.27" width="0.127" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-8.89" y2="0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.255" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-8.89" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-3.175" x2="-8.255" y2="-3.81" width="0.127" layer="21"/>
<text x="-5.715" y="0" size="1.27" layer="21" align="center-left">SCL</text>
<wire x1="-9.652" y1="7.112" x2="9.652" y2="7.112" width="0.127" layer="21"/>
<wire x1="9.652" y1="7.112" x2="9.652" y2="-7.112" width="0.127" layer="21"/>
<wire x1="9.652" y1="-7.112" x2="-9.652" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-9.652" y1="-7.112" x2="-9.652" y2="7.112" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="3.175" x2="-8.89" y2="1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.905" x2="-8.89" y2="-3.175" width="0.127" layer="21"/>
<text x="-5.715" y="2.54" size="1.27" layer="21" align="center-left">SDA</text>
<text x="-5.715" y="5.08" size="1.27" layer="21" align="center-left">ADDR</text>
<text x="-5.715" y="-2.54" size="1.27" layer="21" align="center-left">GND</text>
<wire x1="1.27" y1="1.27" x2="2.794" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.794" y1="1.27" x2="2.794" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.794" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="5" x="-7.62" y="5.08" drill="1"/>
<wire x1="-8.255" y1="-3.81" x2="-8.89" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-5.715" x2="-8.255" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-6.35" x2="-6.985" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-6.35" x2="-6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-5.715" x2="-6.35" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-4.445" x2="-6.985" y2="-3.81" width="0.127" layer="21"/>
<text x="-5.715" y="-5.08" size="1.27" layer="21" align="center-left">VCC</text>
<text x="3.81" y="0" size="1.016" layer="21" align="center-left">BH1750</text>
</package>
<package name="UV-SENSOR-ML8511">
<description>&lt;b&gt;UV  Sensor&lt;/b&gt; based on &lt;b&gt;ML8511&lt;/b&gt; device</description>
<wire x1="-5.08" y1="6.35" x2="-3.81" y2="6.35" width="0.127" layer="21"/>
<text x="0" y="7.62" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.62" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="1" x="-4.445" y="5.08" drill="1" shape="square"/>
<pad name="2" x="-4.445" y="2.54" drill="1"/>
<pad name="3" x="-4.445" y="0" drill="1"/>
<pad name="4" x="-4.445" y="-2.54" drill="1"/>
<wire x1="-3.81" y1="6.35" x2="-3.175" y2="5.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="5.715" x2="-3.175" y2="4.445" width="0.127" layer="21"/>
<wire x1="-3.175" y1="4.445" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="-3.175" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.175" x2="-3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.175" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.905" x2="-3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-3.175" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.715" y1="5.715" x2="-5.08" y2="6.35" width="0.127" layer="21"/>
<wire x1="-5.715" y1="4.445" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.81" x2="-5.715" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="1.905" x2="-5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-0.635" x2="-5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.715" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-3.175" x2="-5.08" y2="-3.81" width="0.127" layer="21"/>
<text x="-2.54" y="0" size="1.27" layer="21" align="center-left">GND</text>
<wire x1="-5.969" y1="6.604" x2="5.969" y2="6.604" width="0.127" layer="21"/>
<wire x1="5.969" y1="6.604" x2="5.969" y2="-6.604" width="0.127" layer="21"/>
<wire x1="5.969" y1="-6.604" x2="-5.969" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-5.969" y1="-6.604" x2="-5.969" y2="6.604" width="0.127" layer="21"/>
<wire x1="-5.715" y1="5.715" x2="-5.715" y2="4.445" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="-5.715" y2="1.905" width="0.127" layer="21"/>
<wire x1="-5.715" y1="0.635" x2="-5.715" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-1.905" x2="-5.715" y2="-3.175" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="21" align="center-left">3V3</text>
<text x="-2.54" y="5.08" size="1.27" layer="21" align="center-left">VIN</text>
<text x="-2.54" y="-2.54" size="1.27" layer="21" align="center-left">OUT</text>
<wire x1="1.524" y1="-0.635" x2="5.334" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.334" y1="-0.635" x2="5.334" y2="-4.445" width="0.127" layer="21"/>
<wire x1="5.334" y1="-4.445" x2="1.524" y2="-4.445" width="0.127" layer="21"/>
<wire x1="1.524" y1="-4.445" x2="1.524" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.81" x2="-5.715" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-4.445" x2="-5.715" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.715" x2="-5.08" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-6.35" x2="-3.81" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-6.35" x2="-3.175" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-5.715" x2="-3.175" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-4.445" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<pad name="5" x="-4.445" y="-5.08" drill="1"/>
<text x="-2.54" y="-5.08" size="1.27" layer="21" align="center-left">EN</text>
</package>
<package name="TEMP-HUM-PRES-BME280">
<description>&lt;b&gt;Digital Humidity-Temperature-Pressure Sensor&lt;/b&gt; based on &lt;b&gt;BME280&lt;/b&gt; device</description>
<wire x1="-5.715" y1="5.08" x2="-4.445" y2="5.08" width="0.127" layer="21"/>
<text x="0" y="6.35" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-6.35" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="1" x="-5.08" y="3.81" drill="1" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="1"/>
<pad name="3" x="-5.08" y="-1.27" drill="1"/>
<pad name="4" x="-5.08" y="-3.81" drill="1"/>
<wire x1="-4.445" y1="5.08" x2="-3.81" y2="4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="4.445" x2="-3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-4.445" y2="2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-4.445" y2="0" width="0.127" layer="21"/>
<wire x1="-4.445" y1="0" x2="-3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-4.445" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-5.715" y2="5.08" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="0" width="0.127" layer="21"/>
<wire x1="-5.715" y1="0" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.35" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-4.445" x2="-5.715" y2="-5.08" width="0.127" layer="21"/>
<text x="-3.175" y="-1.27" size="1.27" layer="21" align="center-left">SCL</text>
<wire x1="-6.604" y1="5.334" x2="6.604" y2="5.334" width="0.127" layer="21"/>
<wire x1="6.604" y1="5.334" x2="6.604" y2="-5.334" width="0.127" layer="21"/>
<wire x1="6.604" y1="-5.334" x2="-6.604" y2="-5.334" width="0.127" layer="21"/>
<wire x1="-6.604" y1="-5.334" x2="-6.604" y2="5.334" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.35" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.08" x2="-4.445" y2="-5.08" width="0.127" layer="21"/>
<text x="-3.175" y="1.27" size="1.27" layer="21" align="center-left">GND</text>
<text x="-3.175" y="3.81" size="1.27" layer="21" align="center-left">VIN</text>
<text x="-3.175" y="-3.81" size="1.27" layer="21" align="center-left">SDA</text>
<wire x1="2.54" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.81" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<circle x="3.048" y="-2.54" radius="0.254" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LIGHT-SENSOR-BH1750">
<description>&lt;b&gt;Digital Light Sensor&lt;/b&gt; based on &lt;b&gt;BH1750&lt;/b&gt;</description>
<pin name="ADDR" x="-17.78" y="5.08" length="middle"/>
<pin name="GND" x="-17.78" y="-2.54" length="middle" direction="pwr"/>
<pin name="SCL" x="-17.78" y="0" length="middle"/>
<pin name="SDA" x="-17.78" y="2.54" length="middle"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="3.302" y1="-2.286" x2="3.302" y2="2.286" width="0.254" layer="94"/>
<wire x1="3.302" y1="2.286" x2="1.016" y2="2.286" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.286" x2="1.016" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.016" y1="-2.286" x2="3.302" y2="-2.286" width="0.254" layer="94"/>
<pin name="VCC" x="-17.78" y="-5.08" length="middle" direction="pwr"/>
<circle x="7.62" y="5.08" radius="2.54" width="0.254" layer="94"/>
<circle x="7.62" y="-5.08" radius="2.54" width="0.254" layer="94"/>
<text x="0" y="0" size="2.032" layer="94" rot="R270" align="top-center">BH1750</text>
</symbol>
<symbol name="UV-SENSOR-ML8511">
<description>&lt;b&gt;UV  Sensor&lt;/b&gt; based on &lt;b&gt;ML8511&lt;/b&gt; device</description>
<pin name="VIN" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="3V3" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="GND" x="-15.24" y="0" length="middle" direction="pwr"/>
<pin name="OUT" x="-15.24" y="-2.54" length="middle"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<pin name="EN" x="-15.24" y="-5.08" length="middle"/>
</symbol>
<symbol name="TEMP-HUM-PRES-BME280">
<description>&lt;b&gt;Digital Humidity-Temperature-Pressure Sensor&lt;/b&gt; based on &lt;b&gt;BME280&lt;/b&gt; device</description>
<pin name="VIN" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="GND" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="SCL" x="-15.24" y="0" length="middle"/>
<pin name="SDA" x="-15.24" y="-2.54" length="middle"/>
<wire x1="-10.16" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<circle x="0.762" y="-2.54" radius="0.359209375" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LIGHT-SENSOR-BH1750">
<description>&lt;b&gt;Digital Light Sensor&lt;/b&gt; based on &lt;b&gt;BH1750&lt;/b&gt;
&lt;p&gt;&lt;b&gt;BH1750&lt;/b&gt; datasheet is available here:&lt;br /&gt;
&lt;a href="http://www.mouser.com/ds/2/348/bh1750fvi-e-186247.pdf"&gt;http://www.mouser.com/ds/2/348/bh1750fvi-e-186247.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Arduino Library&lt;/b&gt; for &lt;b&gt;BH1750&lt;/b&gt; is available here:&lt;br /&gt;
&lt;a href="https://github.com/claws/BH1750"&gt;https://github.com/claws/BH1750&lt;/a&gt;&lt;p/&gt;
&lt;p&gt;&lt;b&gt;Video Tutorial&lt;/b&gt; is available here:&lt;br /&gt;
&lt;a href="https://www.youtube.com/watch?v=UsLcHSbxf_8"&gt;https://www.youtube.com/watch?v=UsLcHSbxf_8&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/bh1750fvi+module"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;br /&gt;
&lt;b&gt;Note:&lt;/b&gt; There are many variants. Search for the proper version.&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=LIGHT-SENSOR-BH1750"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LIGHT-SENSOR-BH1750" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LIGHT-SENSOR-BH1750">
<connects>
<connect gate="G$1" pin="ADDR" pad="5"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UV-SENSOR-ML8511">
<description>&lt;b&gt;UV  Sensor&lt;/b&gt; based on &lt;b&gt;ML8511&lt;/b&gt; device
&lt;p&gt;&lt;b&gt;ML8511&lt;/b&gt; user's guide is available here:&lt;br /&gt;
&lt;a href="https://learn.sparkfun.com/tutorials/ml8511-uv-sensor-hookup-guide/using-the-ml8511"&gt;https://learn.sparkfun.com/tutorials/ml8511-uv-sensor-hookup-guide/using-the-ml8511&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;ML8511&lt;/b&gt; datasheet is available here:&lt;br /&gt;
&lt;a href="https://www.mcs.anl.gov/research/projects/waggle/downloads/datasheets/lightsense/ml8511.pdf"&gt;https://www.mcs.anl.gov/research/projects/waggle/downloads/datasheets/lightsense/ml8511.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/ml8511+module"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=UV-SENSOR-ML8511"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="UV-SENSOR-ML8511" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UV-SENSOR-ML8511">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="OUT" pad="4"/>
<connect gate="G$1" pin="VIN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TEMP-HUM-PRES-BME280">
<description>&lt;b&gt;Digital Humidity-Temperature-Pressure Sensor&lt;/b&gt; based on &lt;b&gt;BME280&lt;/b&gt; device
&lt;p&gt;More information about &lt;b&gt;BME280&lt;/b&gt; device is available here:&lt;br&gt;
&lt;a href="https://www.bosch-sensortec.com/bst/products/all_products/bme280"&gt;https://www.bosch-sensortec.com/bst/products/all_products/bme280&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/bme280+breakout"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=TEMP-HUM-PRES-BME280"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TEMP-HUM-PRES-BME280" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TEMP-HUM-PRES-BME280">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VIN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:13874/1" library_version="1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:13926/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="VCCIO">
<description>&lt;h3&gt;VCC I/O Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCCIO" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="VCC">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCCIO" prefix="SUPPLY">
<description>&lt;h3&gt;VCC I/O Supply&lt;/h3&gt;
&lt;p&gt;Power supply for a chip's input and output pins.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCCIO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="SUPPLY">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;
&lt;p&gt;Positive voltage supply (traditionally for a BJT device, C=collector).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SOSA">
<packages>
</packages>
<symbols>
<symbol name="NC">
<wire x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" width="0.2" layer="91"/>
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.2" layer="91"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Weather Station GEN2">
<packages>
<package name="12_STEPUP">
<pad name="P$2" x="0" y="0" drill="1" shape="square"/>
<pad name="P$3" x="2.54" y="0" drill="1"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<wire x1="-4.05" y1="-1.52" x2="4.05" y2="-1.52" width="0.1524" layer="21"/>
<wire x1="4.05" y1="-1.52" x2="4.05" y2="11.49" width="0.1524" layer="21"/>
<wire x1="4.05" y1="11.49" x2="-4.05" y2="11.49" width="0.1524" layer="21"/>
<wire x1="-4.05" y1="11.49" x2="-4.05" y2="-1.52" width="0.1524" layer="21"/>
<text x="0" y="1.27" size="1.4224" layer="21" rot="R90" align="center-left">GND</text>
<text x="2.54" y="1.27" size="1.4224" layer="21" rot="R90" align="center-left">VOUT</text>
<text x="-2.54" y="1.27" size="1.4224" layer="21" rot="R90" align="center-left">VIN</text>
</package>
<package name="DCDC_5V3A">
<wire x1="-10.95" y1="-7.05" x2="10.95" y2="-7.05" width="0.1524" layer="21"/>
<wire x1="10.95" y1="-7.05" x2="10.95" y2="7.05" width="0.1524" layer="21"/>
<wire x1="10.95" y1="7.05" x2="-10.95" y2="7.05" width="0.1524" layer="21"/>
<wire x1="-10.95" y1="7.05" x2="-10.95" y2="-7.05" width="0.1524" layer="21"/>
<pad name="P$1" x="-9" y="5.12" drill="1.5" shape="square"/>
<pad name="P$4" x="9" y="-4.93" drill="1.5" shape="square"/>
<pad name="P$2" x="-9" y="-5.12" drill="1.5" shape="square"/>
<pad name="P$3" x="9" y="4.93" drill="1.5" shape="square"/>
<text x="-7.5" y="5.1" size="1.778" layer="21" align="center-left">VIN</text>
<text x="-7.5" y="-5.15" size="1.778" layer="21" align="center-left">GND</text>
<text x="7.5" y="-4.95" size="1.778" layer="21" align="center-right">GND</text>
<text x="7.5" y="4.9" size="1.778" layer="21" align="center-right">VOUT</text>
<text x="-11" y="8" size="1.778" layer="25">&gt;NAME</text>
</package>
<package name="2PIN_CONN">
<pad name="P$1" x="-2.54" y="0" drill="1" diameter="1.778"/>
<pad name="P$2" x="2.54" y="0" drill="1" diameter="1.778"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<text x="-3.81" y="1.524" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="3PIN_CONN">
<pad name="P$1" x="-5.08" y="0" drill="1" diameter="1.778"/>
<pad name="P$2" x="0" y="0" drill="1" diameter="1.778"/>
<pad name="P$3" x="5.08" y="0" drill="1" diameter="1.778"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="6.35" y2="-1.27" width="0.127" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="1.27" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.27" x2="-6.35" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="1.778" size="1.016" layer="21" align="bottom-center">&gt;NAME</text>
</package>
<package name="CJMCU-811">
<wire x1="-10.5" y1="9" x2="-10.5" y2="-9" width="0.1524" layer="21"/>
<wire x1="-10.5" y1="-9" x2="10.5" y2="-9" width="0.1524" layer="21"/>
<wire x1="10.5" y1="-9" x2="10.5" y2="9" width="0.1524" layer="21"/>
<wire x1="10.5" y1="9" x2="-10.5" y2="9" width="0.1524" layer="21"/>
<pad name="4" x="-1.27" y="7.62" drill="1" diameter="1.778"/>
<pad name="5" x="1.27" y="7.62" drill="1" diameter="1.778"/>
<pad name="3" x="-3.81" y="7.62" drill="1" diameter="1.778"/>
<pad name="6" x="3.81" y="7.62" drill="1" diameter="1.778"/>
<pad name="2" x="-6.35" y="7.62" drill="1" diameter="1.778"/>
<pad name="7" x="6.35" y="7.62" drill="1" diameter="1.778"/>
<pad name="1" x="-8.89" y="7.62" drill="1" diameter="1.778"/>
<pad name="8" x="8.89" y="7.62" drill="1" diameter="1.778"/>
<text x="8.89" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">VCC</text>
<text x="6.35" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">GND</text>
<text x="3.81" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">SCL</text>
<text x="1.27" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">SDA</text>
<text x="-1.27" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">WAK</text>
<text x="-3.81" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">INT</text>
<text x="-6.35" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">RST</text>
<text x="-8.89" y="6.35" size="1.27" layer="21" rot="R90" align="center-right">ADD</text>
<text x="0" y="-2.54" size="1.6764" layer="21" rot="SR180" align="center">CJMCU-811</text>
<text x="-11.43" y="-8.89" size="1.6764" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="DCDC_CONVERTER">
<pin name="GND" x="0" y="-10.16" visible="pin" length="middle" rot="R90"/>
<pin name="VIN" x="-5.08" y="12.7" visible="pin" length="middle" rot="R270"/>
<pin name="VOUT" x="5.08" y="12.7" visible="pin" length="middle" rot="R270"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-7.62" y2="-5.08" width="0.1524" layer="94"/>
<text x="-8.128" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="POWER_CONN">
<pin name="POW" x="-7.62" y="2.54" visible="pin" length="middle"/>
<pin name="GND" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="-5.08" width="0.254" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<text x="-2.54" y="5.842" size="1.4224" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TEMP_CONN">
<pin name="VCC" x="-7.62" y="7.62" visible="pin" length="middle"/>
<pin name="DAT" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<pin name="GND" x="-7.62" y="-7.62" visible="pin" length="middle"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="6.35" y2="10.16" width="0.254" layer="94"/>
<wire x1="6.35" y1="10.16" x2="6.35" y2="-10.16" width="0.254" layer="94"/>
<wire x1="6.35" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<text x="-2.54" y="10.922" size="1.4224" layer="95">&gt;NAME</text>
</symbol>
<symbol name="CJMCU-811">
<pin name="INT" x="-13.97" y="-1.27" length="middle"/>
<pin name="RST" x="-13.97" y="-3.81" length="middle"/>
<pin name="ADD" x="-13.97" y="-6.35" length="middle"/>
<pin name="WAK" x="-13.97" y="1.27" length="middle"/>
<pin name="SCL" x="-13.97" y="3.81" length="middle"/>
<pin name="SDA" x="-13.97" y="6.35" length="middle"/>
<pin name="VCC" x="-13.97" y="8.89" length="middle"/>
<pin name="GND" x="-13.97" y="-8.89" length="middle"/>
<wire x1="-8.89" y1="11.43" x2="-8.89" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-11.43" x2="8.89" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-11.43" x2="8.89" y2="11.43" width="0.1524" layer="94"/>
<wire x1="8.89" y1="11.43" x2="-8.89" y2="11.43" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.6764" layer="94" rot="R270" align="center">CJMCU-811</text>
<text x="-8.89" y="12.7" size="1.6764" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DCDC_CONVERTER">
<gates>
<gate name="G$1" symbol="DCDC_CONVERTER" x="0" y="-1.27"/>
</gates>
<devices>
<device name="12V" package="12_STEPUP">
<connects>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="VIN" pad="P$1"/>
<connect gate="G$1" pin="VOUT" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="DCDC_5V3A">
<connects>
<connect gate="G$1" pin="GND" pad="P$2 P$4"/>
<connect gate="G$1" pin="VIN" pad="P$1"/>
<connect gate="G$1" pin="VOUT" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER_CONN">
<gates>
<gate name="G$1" symbol="POWER_CONN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2PIN_CONN">
<connects>
<connect gate="G$1" pin="GND" pad="P$1"/>
<connect gate="G$1" pin="POW" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TEMP_CONN">
<gates>
<gate name="G$1" symbol="TEMP_CONN" x="0" y="0"/>
</gates>
<devices>
<device name="3PIN_CONN" package="3PIN_CONN">
<connects>
<connect gate="G$1" pin="DAT" pad="P$3"/>
<connect gate="G$1" pin="GND" pad="P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CJMCU-811">
<gates>
<gate name="G$1" symbol="CJMCU-811" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CJMCU-811">
<connects>
<connect gate="G$1" pin="ADD" pad="1"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="INT" pad="3"/>
<connect gate="G$1" pin="RST" pad="2"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WAK" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="EIA3216">
<description>Generic EIA 3216 (1206) polarized tantalum capacitor</description>
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="51"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="51"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="51"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="51"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.2032" layer="21"/>
<smd name="-" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="+" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.286" y1="0.9906" x2="2.286" y2="0.9906" width="0.0508" layer="39"/>
<wire x1="2.286" y1="0.9906" x2="2.286" y2="-0.9906" width="0.0508" layer="39"/>
<wire x1="2.286" y1="-0.9906" x2="-2.286" y2="-0.9906" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="-0.9906" x2="-2.286" y2="0.9906" width="0.0508" layer="39"/>
</package>
<package name="0603-POLAR">
<description>&lt;p&gt;&lt;b&gt;Polarized 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.1" y1="-0.8" x2="-1.7" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="-1.1" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-0.8" x2="1.5" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-0.8" x2="1.9" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="1.9" y1="-0.4" x2="1.9" y2="0.4" width="0.2032" layer="51"/>
<wire x1="1.9" y1="0.4" x2="1.5" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.5" y1="0.8" x2="1.1" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="-" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="+" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="1.651" y1="0.508" x2="1.651" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="EIA3528">
<description>Generic EIA 3528 polarized tantalum capacitor</description>
<wire x1="-0.9" y1="-1.6" x2="-2.6" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-1.6" x2="-2.6" y2="1.55" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.55" x2="2.2" y2="-1.55" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-1.55" x2="2.6" y2="-1.2" width="0.2032" layer="51"/>
<wire x1="2.6" y1="-1.2" x2="2.6" y2="1.25" width="0.2032" layer="51"/>
<wire x1="2.6" y1="1.25" x2="2.2" y2="1.55" width="0.2032" layer="51"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="51"/>
<wire x1="2.641" y1="1.311" x2="2.641" y2="-1.286" width="0.2032" layer="21" style="longdash"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<text x="0" y="1.778" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="CPOL-RADIAL-2.5MM-5MM">
<description>2.5 mm spaced PTHs with 5 mm diameter outline and standard solder mask</description>
<pad name="1" x="1.25" y="0" drill="0.7" diameter="1.651" shape="square"/>
<pad name="2" x="-1.25" y="0" drill="0.7" diameter="1.651"/>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-0.742" y1="1.397" x2="-1.758" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.758" y1="1.397" x2="0.742" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.25" y1="1.905" x2="1.25" y2="0.889" width="0.2032" layer="21"/>
</package>
<package name="CPOL-RADIAL-2.5MM-5MM-KIT">
<description>2.5 mm spaced PTHs with top copper masked</description>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<pad name="1" x="1.25" y="0" drill="0.7" diameter="1.651" shape="square" stop="no"/>
<pad name="2" x="-1.25" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.25" y="0" radius="0.3556" width="0" layer="29"/>
<circle x="-1.25" y="0" radius="0.9652" width="0" layer="30"/>
<circle x="1.25" y="0" radius="0.3556" width="0" layer="29"/>
<rectangle x1="0.2848" y1="-0.9652" x2="2.2152" y2="0.9652" layer="30"/>
<wire x1="-0.742" y1="1.397" x2="-1.758" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.758" y1="1.397" x2="0.742" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.25" y1="1.905" x2="1.25" y2="0.889" width="0.2032" layer="21"/>
</package>
<package name="EIA6032-NOM">
<description>Metric Size Code EIA 6032-25 Median (Nominal) Land Protrusion&lt;br /&gt;
http://www.kemet.com/Lists/ProductCatalog/Attachments/254/KEM_T2005_T491.pdf</description>
<wire x1="-3.91" y1="1.5" x2="-2" y2="1.5" width="0.127" layer="51"/>
<wire x1="-3.91" y1="1.5" x2="-3.91" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-3.91" y1="-1.5" x2="-2" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2" y1="1.5" x2="3.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="1.5" x2="3.91" y2="1" width="0.127" layer="51"/>
<wire x1="3.91" y1="1" x2="3.91" y2="-1" width="0.127" layer="51"/>
<wire x1="3.91" y1="-1" x2="3.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-1.5" x2="2" y2="-1.5" width="0.127" layer="51"/>
<smd name="C" x="-2.47" y="0" dx="2.37" dy="2.23" layer="1" rot="R180"/>
<smd name="A" x="2.47" y="0" dx="2.37" dy="2.23" layer="1" rot="R180"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="3.91" y1="1" x2="3.91" y2="-1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1.0UF" prefix="C">
<description>&lt;h3&gt;1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-16V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00868"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12417"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11625"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09822"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08064"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0603-16V-10%-X7R" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13930"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10UF-POLAR" prefix="C">
<description>&lt;h3&gt;10.0µF polarized capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="-EIA3216-16V-10%(TANT)" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00811"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-0603-6.3V-20%(TANT)" package="0603-POLAR">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13210"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-EIA3528-20V-10%(TANT)" package="EIA3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08063"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-RADIAL-2.5MM-25V-20%" package="CPOL-RADIAL-2.5MM-5MM">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08440"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-RADIAL-2.5MM-KIT-25V-20%" package="CPOL-RADIAL-2.5MM-5MM-KIT">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08440"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-EIA6032-25V-10%" package="EIA6032-NOM">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12984"/>
<attribute name="VALUE" value="10µF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="esp32-environ-monitor-temp">
<packages>
<package name="SOT-23-6">
<wire x1="1.4224" y1="0.4294" x2="1.4224" y2="-0.4294" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.4294" x2="-1.4224" y2="0.4294" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1524" layer="51"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="-1.3751" dx="0.55" dy="1.35" layer="1"/>
<smd name="2" x="0" y="-1.3751" dx="0.55" dy="1.35" layer="1"/>
<smd name="3" x="0.95" y="-1.3751" dx="0.55" dy="1.35" layer="1"/>
<smd name="4" x="0.95" y="1.3751" dx="0.55" dy="1.35" layer="1"/>
<smd name="5" x="0" y="1.3751" dx="0.55" dy="1.35" layer="1"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<smd name="6" x="-0.95" y="1.3751" dx="0.55" dy="1.35" layer="1"/>
<rectangle x1="-0.25" y1="0.85" x2="0.25" y2="1.5" layer="51"/>
<circle x="-1.65" y="-1" radius="0.1436" width="0.2032" layer="21"/>
<text x="-1.905" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="2.54" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
</package>
<package name="RJ11-6">
<description>&lt;h3&gt;RJ11 6-Pin Socket with PCB Mounting Post&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 6&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/RJ11-Datasheet.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;RJ11-6&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.858" y1="-5.207" x2="6.858" y2="10.414" width="0.2032" layer="21"/>
<wire x1="6.858" y1="10.414" x2="-6.858" y2="10.414" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="10.414" x2="-6.858" y2="-5.207" width="0.2032" layer="21"/>
<wire x1="6.858" y1="-5.207" x2="-6.858" y2="-5.207" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="-5.207" x2="-6.858" y2="-8.128" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="-8.128" x2="6.858" y2="-8.128" width="0.2032" layer="21"/>
<wire x1="6.858" y1="-8.128" x2="6.858" y2="-5.207" width="0.2032" layer="21"/>
<pad name="1" x="-3.175" y="8.89" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-1.905" y="6.35" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-0.635" y="8.89" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0.635" y="6.35" drill="1.016" diameter="1.8796"/>
<pad name="5" x="1.905" y="8.89" drill="1.016" diameter="1.8796"/>
<pad name="6" x="3.175" y="6.35" drill="1.016" diameter="1.8796"/>
<hole x="-5.08" y="0" drill="3.2512"/>
<hole x="5.08" y="0" drill="3.2512"/>
<text x="-1.3208" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TVS_DIODE_ARRAY">
<wire x1="-5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="2.54" y="-10.16" size="1.778" layer="95">&gt;Name</text>
<text x="2.54" y="-12.7" size="1.778" layer="96">&gt;Value</text>
<pin name="IO1" x="-2.54" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="IO2" x="2.54" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="IO4" x="-2.54" y="7.62" visible="off" length="short" rot="R270"/>
<pin name="VCC" x="0" y="7.62" visible="off" length="short" rot="R270"/>
<pin name="IO3" x="2.54" y="7.62" visible="off" length="short" rot="R270"/>
<polygon width="0.0254" layer="94">
<vertex x="-0.635" y="-0.3048"/>
<vertex x="0" y="0.3302"/>
<vertex x="0.635" y="-0.3048"/>
</polygon>
<wire x1="0.3302" y1="0.3302" x2="0.4318" y2="0.4318" width="0.0254" layer="94"/>
<wire x1="-0.3302" y1="0.3302" x2="0.3302" y2="0.3302" width="0.0254" layer="94"/>
<wire x1="-0.4318" y1="0.2286" x2="-0.3302" y2="0.3302" width="0.0254" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="-4.445" y="2.2352"/>
<vertex x="-3.81" y="2.8702"/>
<vertex x="-3.175" y="2.2352"/>
</polygon>
<wire x1="-3.4798" y1="2.8702" x2="-3.3782" y2="2.9718" width="0.0254" layer="94"/>
<wire x1="-4.1402" y1="2.8702" x2="-3.4798" y2="2.8702" width="0.0254" layer="94"/>
<wire x1="-4.2418" y1="2.7686" x2="-4.1402" y2="2.8702" width="0.0254" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="-1.905" y="2.2352"/>
<vertex x="-1.27" y="2.8702"/>
<vertex x="-0.635" y="2.2352"/>
</polygon>
<wire x1="-0.9398" y1="2.8702" x2="-0.8382" y2="2.9718" width="0.0254" layer="94"/>
<wire x1="-1.6002" y1="2.8702" x2="-0.9398" y2="2.8702" width="0.0254" layer="94"/>
<wire x1="-1.7018" y1="2.7686" x2="-1.6002" y2="2.8702" width="0.0254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="0.254" width="0.0762" layer="94"/>
<wire x1="-1.27" y1="0.254" x2="-1.27" y2="-3.81" width="0.0762" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="-0.254" width="0.0762" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="-4.445" y="-2.8448"/>
<vertex x="-3.81" y="-2.2098"/>
<vertex x="-3.175" y="-2.8448"/>
</polygon>
<wire x1="-3.81" y1="-0.254" x2="-3.81" y2="-3.81" width="0.0762" layer="94"/>
<wire x1="-3.4798" y1="-2.2098" x2="-3.3782" y2="-2.1082" width="0.0254" layer="94"/>
<wire x1="-4.1402" y1="-2.2098" x2="-3.4798" y2="-2.2098" width="0.0254" layer="94"/>
<wire x1="-4.2418" y1="-2.3114" x2="-4.1402" y2="-2.2098" width="0.0254" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="-1.905" y="-2.8448"/>
<vertex x="-1.27" y="-2.2098"/>
<vertex x="-0.635" y="-2.8448"/>
</polygon>
<wire x1="-0.9398" y1="-2.2098" x2="-0.8382" y2="-2.1082" width="0.0254" layer="94"/>
<wire x1="-1.6002" y1="-2.2098" x2="-0.9398" y2="-2.2098" width="0.0254" layer="94"/>
<wire x1="-1.7018" y1="-2.3114" x2="-1.6002" y2="-2.2098" width="0.0254" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="0.635" y="2.2352"/>
<vertex x="1.27" y="2.8702"/>
<vertex x="1.905" y="2.2352"/>
</polygon>
<wire x1="1.6002" y1="2.8702" x2="1.7018" y2="2.9718" width="0.0254" layer="94"/>
<wire x1="0.9398" y1="2.8702" x2="1.6002" y2="2.8702" width="0.0254" layer="94"/>
<wire x1="0.8382" y1="2.7686" x2="0.9398" y2="2.8702" width="0.0254" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="3.175" y="2.2352"/>
<vertex x="3.81" y="2.8702"/>
<vertex x="4.445" y="2.2352"/>
</polygon>
<wire x1="4.1402" y1="2.8702" x2="4.2418" y2="2.9718" width="0.0254" layer="94"/>
<wire x1="3.4798" y1="2.8702" x2="4.1402" y2="2.8702" width="0.0254" layer="94"/>
<wire x1="3.3782" y1="2.7686" x2="3.4798" y2="2.8702" width="0.0254" layer="94"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="0.254" width="0.0762" layer="94"/>
<wire x1="3.81" y1="0.254" x2="3.81" y2="-3.81" width="0.0762" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="-0.254" width="0.0762" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="0.635" y="-2.8448"/>
<vertex x="1.27" y="-2.2098"/>
<vertex x="1.905" y="-2.8448"/>
</polygon>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="-3.81" width="0.0762" layer="94"/>
<wire x1="1.6002" y1="-2.2098" x2="1.7018" y2="-2.1082" width="0.0254" layer="94"/>
<wire x1="0.9398" y1="-2.2098" x2="1.6002" y2="-2.2098" width="0.0254" layer="94"/>
<wire x1="0.8382" y1="-2.3114" x2="0.9398" y2="-2.2098" width="0.0254" layer="94"/>
<polygon width="0.0254" layer="94">
<vertex x="3.175" y="-2.8448"/>
<vertex x="3.81" y="-2.2098"/>
<vertex x="4.445" y="-2.8448"/>
</polygon>
<wire x1="4.1402" y1="-2.2098" x2="4.2418" y2="-2.1082" width="0.0254" layer="94"/>
<wire x1="3.4798" y1="-2.2098" x2="4.1402" y2="-2.2098" width="0.0254" layer="94"/>
<wire x1="3.3782" y1="-2.3114" x2="3.4798" y2="-2.2098" width="0.0254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.0762" layer="94"/>
<wire x1="2.54" y1="-0.254" x2="1.27" y2="-0.254" width="0.0762" layer="94"/>
<wire x1="2.54" y1="-0.254" x2="2.54" y2="-5.08" width="0.0762" layer="94"/>
<wire x1="-3.81" y1="-0.254" x2="-2.54" y2="-0.254" width="0.0762" layer="94"/>
<wire x1="-2.54" y1="-0.254" x2="-2.54" y2="-5.08" width="0.0762" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="0.254" width="0.0762" layer="94"/>
<wire x1="-2.54" y1="0.254" x2="-1.27" y2="0.254" width="0.0762" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="0.254" width="0.0762" layer="94"/>
<wire x1="2.54" y1="0.254" x2="3.81" y2="0.254" width="0.0762" layer="94"/>
<circle x="-3.81" y="-0.254" radius="0.0254" width="0.254" layer="94"/>
<circle x="-1.27" y="0.254" radius="0.0254" width="0.254" layer="94"/>
<circle x="1.27" y="-0.254" radius="0.0254" width="0.254" layer="94"/>
<circle x="3.81" y="0.254" radius="0.0254" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="3.81" y2="-3.81" width="0.0762" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.0762" layer="94"/>
<circle x="-1.27" y="3.81" radius="0.0254" width="0.254" layer="94"/>
<circle x="1.27" y="3.81" radius="0.0254" width="0.254" layer="94"/>
<circle x="1.27" y="-3.81" radius="0.0254" width="0.254" layer="94"/>
<circle x="-1.27" y="-3.81" radius="0.0254" width="0.254" layer="94"/>
<circle x="0" y="-3.81" radius="0.0254" width="0.254" layer="94"/>
<circle x="0" y="3.81" radius="0.0254" width="0.254" layer="94"/>
</symbol>
<symbol name="WEATHER_METER-RAIN">
<description>&lt;h3&gt;Weather Meter - Rain Gauge Symbol&lt;/h3&gt;
&lt;p&gt;The rain half of the &lt;a href=https://www.sparkfun.com/products/8942&gt;SparkFun Weather Meter&lt;/a&gt;. The rain gauge is a self-emptying tipping bucket type.  Each 0.011” (0.2794 mm) of rain causes one momentary contact closure that can be recorded with a digital counter or microcontroller interrupt input.  The gauge’s switch is connected to the two center conductors of the attached RJ11-terminated cable.&lt;/p&gt;
&lt;p&gt;&lt;a href=http://www.sparkfun.com/datasheets/Sensors/Weather/Weather%20Sensor%20Assembly..pdf&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<text x="-10.16" y="-5.334" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-10.16" y="5.334" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="-12.7" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="12.7" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="4.572" size="1.524" layer="94" font="vector" ratio="10" align="top-center">Rain Gauge</text>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="0" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-5.08" y2="2.54" width="0.1524" layer="94" curve="-90"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="0" width="0.1524" layer="94" curve="-90"/>
<wire x1="7.62" y1="0" x2="5.08" y2="-2.54" width="0.1524" layer="94" curve="-90"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-7.62" y2="0" width="0.1524" layer="94" curve="-90"/>
</symbol>
<symbol name="WEATHER_METER-WIND">
<description>&lt;h3&gt;Weather Meter - Anemometer/ Wind Direction Symbol&lt;/h3&gt;
&lt;p&gt;The wind half of the &lt;a href=https://www.sparkfun.com/products/8942&gt;SparkFun Weather Meter&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;The cup-type anemometer measures wind speed by closing a contact as a magnet moves past a switch.  A wind speed of 1.492 MPH (2.4 km/h) causes the switch to close once per second. The anemometer switch is connected to the inner two conductors of the RJ11 cable shared by the anemometer and wind vane (pins 2 and 3.) &lt;/p&gt;
&lt;p&gt;The wind vane is the most complicated of the three sensors.  It has eight switches, each connected to a different resistor.  The vane’s magnet may close two switches at once, allowing up to 16 different positions to be indicated.  An external resistor can be used to form a voltage divider, producing a voltage output that can be measured with an analog to digital converter. &lt;/p&gt;
&lt;p&gt;&lt;a href=http://www.sparkfun.com/datasheets/Sensors/Weather/Weather%20Sensor%20Assembly..pdf&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="ANN-2" x="12.7" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="ANN-1" x="-12.7" y="-7.62" visible="pad" length="short"/>
<pin name="DIR2" x="12.7" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="DIR-1" x="-12.7" y="2.54" visible="pad" length="short"/>
<text x="0" y="-3.048" size="1.524" layer="94" font="vector" ratio="10" align="top-center">Anemometer</text>
<text x="0" y="7.112" size="1.524" layer="94" font="vector" ratio="10" align="top-center">Wind Vane</text>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="5.08" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="0" y2="-6.096" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.635" y1="2.54" x2="-0.762" y2="2.794" width="0.1016" layer="94"/>
<wire x1="-1.016" y1="2.286" x2="-0.762" y2="2.794" width="0.1016" layer="94"/>
<wire x1="-1.016" y1="2.286" x2="-1.27" y2="2.794" width="0.1016" layer="94"/>
<wire x1="-1.524" y1="2.286" x2="-1.27" y2="2.794" width="0.1016" layer="94"/>
<wire x1="-1.524" y1="2.286" x2="-1.778" y2="2.794" width="0.1016" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.778" y2="2.794" width="0.1016" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-2.159" y2="2.54" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.635" y2="2.54" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="0.635" y2="2.54" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.1016" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.1016" layer="94"/>
<circle x="0" y="2.54" radius="2.54" width="0.1016" layer="94"/>
<wire x1="-0.762" y1="2.921" x2="-0.508" y2="3.048" width="0.1016" layer="94"/>
<wire x1="-0.508" y1="3.429" x2="-0.762" y2="2.921" width="0.1016" layer="94"/>
<wire x1="-0.508" y1="3.429" x2="-1.016" y2="3.175" width="0.1016" layer="94"/>
<wire x1="-0.762" y1="3.683" x2="-1.016" y2="3.175" width="0.1016" layer="94"/>
<wire x1="-0.762" y1="3.683" x2="-1.27" y2="3.429" width="0.1016" layer="94"/>
<wire x1="-1.143" y1="3.683" x2="-1.27" y2="3.429" width="0.1016" layer="94"/>
<wire x1="-1.143" y1="3.683" x2="-1.524" y2="4.064" width="0.1016" layer="94"/>
<wire x1="0" y1="3.175" x2="0.254" y2="3.302" width="0.1016" layer="94"/>
<wire x1="-0.254" y1="3.556" x2="0.254" y2="3.302" width="0.1016" layer="94"/>
<wire x1="-0.254" y1="3.556" x2="0.254" y2="3.81" width="0.1016" layer="94"/>
<wire x1="-0.254" y1="4.064" x2="0.254" y2="3.81" width="0.1016" layer="94"/>
<wire x1="-0.254" y1="4.064" x2="0.254" y2="4.318" width="0.1016" layer="94"/>
<wire x1="0" y1="4.445" x2="0.254" y2="4.318" width="0.1016" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="4.826" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="0.508" y2="3.048" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.508" y2="3.048" width="0.1016" layer="94"/>
<wire x1="0.381" y1="3.302" x2="0.508" y2="3.048" width="0.1016" layer="94"/>
<wire x1="0.889" y1="3.048" x2="0.381" y2="3.302" width="0.1016" layer="94"/>
<wire x1="0.889" y1="3.048" x2="0.635" y2="3.556" width="0.1016" layer="94"/>
<wire x1="1.143" y1="3.302" x2="0.635" y2="3.556" width="0.1016" layer="94"/>
<wire x1="1.143" y1="3.302" x2="0.889" y2="3.81" width="0.1016" layer="94"/>
<wire x1="1.143" y1="3.683" x2="0.889" y2="3.81" width="0.1016" layer="94"/>
<wire x1="1.143" y1="3.683" x2="1.524" y2="4.064" width="0.1016" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.762" y2="2.286" width="0.1016" layer="94"/>
<wire x1="1.016" y1="2.794" x2="0.762" y2="2.286" width="0.1016" layer="94"/>
<wire x1="1.016" y1="2.794" x2="1.27" y2="2.286" width="0.1016" layer="94"/>
<wire x1="1.524" y1="2.794" x2="1.27" y2="2.286" width="0.1016" layer="94"/>
<wire x1="1.524" y1="2.794" x2="1.778" y2="2.286" width="0.1016" layer="94"/>
<wire x1="1.905" y1="2.54" x2="1.778" y2="2.286" width="0.1016" layer="94"/>
<wire x1="1.905" y1="2.54" x2="2.159" y2="2.54" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="0.508" y2="2.032" width="0.1016" layer="94"/>
<wire x1="0.762" y1="2.159" x2="0.508" y2="2.032" width="0.1016" layer="94"/>
<wire x1="0.508" y1="1.651" x2="0.762" y2="2.159" width="0.1016" layer="94"/>
<wire x1="0.508" y1="1.651" x2="1.016" y2="1.905" width="0.1016" layer="94"/>
<wire x1="0.762" y1="1.397" x2="1.016" y2="1.905" width="0.1016" layer="94"/>
<wire x1="0.762" y1="1.397" x2="1.27" y2="1.651" width="0.1016" layer="94"/>
<wire x1="1.143" y1="1.397" x2="1.27" y2="1.651" width="0.1016" layer="94"/>
<wire x1="1.143" y1="1.397" x2="1.524" y2="1.016" width="0.1016" layer="94"/>
<wire x1="0" y1="1.905" x2="-0.254" y2="1.778" width="0.1016" layer="94"/>
<wire x1="0.254" y1="1.524" x2="-0.254" y2="1.778" width="0.1016" layer="94"/>
<wire x1="0.254" y1="1.524" x2="-0.254" y2="1.27" width="0.1016" layer="94"/>
<wire x1="0.254" y1="1.016" x2="-0.254" y2="1.27" width="0.1016" layer="94"/>
<wire x1="0.254" y1="1.016" x2="-0.254" y2="0.762" width="0.1016" layer="94"/>
<wire x1="0" y1="0.635" x2="-0.254" y2="0.762" width="0.1016" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="0.381" width="0.1016" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.508" y2="2.032" width="0.1016" layer="94"/>
<wire x1="-0.381" y1="1.778" x2="-0.508" y2="2.032" width="0.1016" layer="94"/>
<wire x1="-0.889" y1="2.032" x2="-0.381" y2="1.778" width="0.1016" layer="94"/>
<wire x1="-0.889" y1="2.032" x2="-0.635" y2="1.524" width="0.1016" layer="94"/>
<wire x1="-1.143" y1="1.778" x2="-0.635" y2="1.524" width="0.1016" layer="94"/>
<wire x1="-1.143" y1="1.778" x2="-0.889" y2="1.27" width="0.1016" layer="94"/>
<wire x1="-1.143" y1="1.397" x2="-0.889" y2="1.27" width="0.1016" layer="94"/>
<wire x1="-1.143" y1="1.397" x2="-1.524" y2="1.016" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="-0.254" y2="0.381" width="0.1016" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.159" y2="2.286" width="0.1016" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.159" y2="2.794" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0.254" y2="4.699" width="0.1016" layer="94"/>
<wire x1="-1.778" y1="4.318" x2="-1.397" y2="4.191" width="0.1016" layer="94"/>
<wire x1="1.778" y1="4.318" x2="1.651" y2="3.937" width="0.1016" layer="94"/>
<wire x1="1.778" y1="0.762" x2="1.397" y2="0.889" width="0.1016" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-1.651" y2="1.143" width="0.1016" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-10.16" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="10.16" y2="2.54" width="0.1524" layer="94"/>
<text x="0" y="5.207" size="0.8128" layer="94" font="vector" align="bottom-center">N</text>
<text x="0" y="-0.127" size="0.8128" layer="94" font="vector" align="top-center">S</text>
<text x="2.667" y="2.667" size="0.8128" layer="94" font="vector">E</text>
<text x="-2.667" y="2.667" size="0.8128" layer="94" font="vector" align="bottom-right">W</text>
<text x="-10.16" y="-12.954" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-10.16" y="7.874" size="1.778" layer="95" font="vector">&gt;NAME</text>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-5.08" y2="-5.08" width="0.1524" layer="94" curve="-90"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="7.62" y2="-7.62" width="0.1524" layer="94" curve="-90"/>
<wire x1="7.62" y1="-7.62" x2="5.08" y2="-10.16" width="0.1524" layer="94" curve="-90"/>
<wire x1="5.08" y1="-10.16" x2="-5.08" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-7.62" y2="-7.62" width="0.1524" layer="94" curve="-90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TVS_DIODE_ARRAY" prefix="U">
<gates>
<gate name="U1" symbol="TVS_DIODE_ARRAY" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="SOT-23-6">
<connects>
<connect gate="U1" pin="IO1" pad="1"/>
<connect gate="U1" pin="IO2" pad="3"/>
<connect gate="U1" pin="IO3" pad="4"/>
<connect gate="U1" pin="IO4" pad="6"/>
<connect gate="U1" pin="VCC" pad="5"/>
<connect gate="U1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-13538" constant="no"/>
<attribute name="VALUE" value="ESD Diode" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WEATHER_METER-RAIN" prefix="J">
<description>&lt;h3&gt;Weather Meter - Rain Gauge Symbol&lt;/h3&gt;
&lt;p&gt;The rain half of the &lt;a href=https://www.sparkfun.com/products/8942&gt;SparkFun Weather Meter&lt;/a&gt;. The rain gauge is a self-emptying tipping bucket type.  Each 0.011” (0.2794 mm) of rain causes one momentary contact closure that can be recorded with a digital counter or microcontroller interrupt input.  The gauge’s switch is connected to the two center conductors of the attached RJ11-terminated cable.&lt;/p&gt;
&lt;p&gt;&lt;a href=http://www.sparkfun.com/datasheets/Sensors/Weather/Weather%20Sensor%20Assembly..pdf&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/132"&gt;RJ11 6-Pin Connector&lt;/a&gt; (PRT-00132)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/Connectors/RJ11-Datasheet.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;
&lt;b&gt;It is used on this SparkFun product:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12081"&gt;SparkFun Weather Shield&lt;/a&gt; (DEV-12081)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8942"&gt;Weather Meters&lt;/a&gt; (SEN-08942)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="WEATHER_METER-RAIN" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="RJ11-6">
<connects>
<connect gate="G$1" pin="1" pad="3"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08590"/>
<attribute name="SF_ID" value="SEN-08942"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WEATHER_METER-WIND" prefix="J">
<description>&lt;h3&gt;RJ11 Jack - 6 pin&lt;/h3&gt;
Commonly found on CAT3 devices and some Microchip programmers.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/132"&gt;RJ11 6-Pin Connector&lt;/a&gt; (PRT-00132)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/Connectors/RJ11-Datasheet.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;It is used on this SparkFun product:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12081"&gt;SparkFun Weather Shield&lt;/a&gt; (DEV-12081)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8942"&gt;Weather Meters&lt;/a&gt; (SEN-08942)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="WEATHER_METER-WIND" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RJ11-6">
<connects>
<connect gate="G$1" pin="ANN-1" pad="3"/>
<connect gate="G$1" pin="ANN-2" pad="4"/>
<connect gate="G$1" pin="DIR-1" pad="2"/>
<connect gate="G$1" pin="DIR2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08590" constant="no"/>
<attribute name="SF_ID" value="PRT-00132" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="AXIAL-0.3">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1">
<description>&lt;h3&gt;AXIAL-0.1&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="21" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1-KIT">
<description>&lt;h3&gt;AXIAL-0.1-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.1 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1KOHM" prefix="R">
<description>&lt;h3&gt;1kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09769"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07856"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10KOHM" prefix="R">
<description>&lt;h3&gt;10kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/6W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/6W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/6W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00824"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="vcc" width="0.254" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="U3" library="diy-modules" deviceset="LIGHT-SENSOR-BH1750" device=""/>
<part name="U4" library="diy-modules" deviceset="UV-SENSOR-ML8511" device=""/>
<part name="U5" library="diy-modules" deviceset="TEMP-HUM-PRES-BME280" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="U$6" library="SOSA" deviceset="NC" device=""/>
<part name="U$7" library="SOSA" deviceset="NC" device=""/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="U7" library="Weather Station GEN2" deviceset="DCDC_CONVERTER" device=""/>
<part name="J1" library="Weather Station GEN2" deviceset="POWER_CONN" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY4" library="SparkFun-PowerSymbols" deviceset="VCC" device=""/>
<part name="J2" library="Weather Station GEN2" deviceset="TEMP_CONN" device="3PIN_CONN"/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="GND6" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="U1" library="ESP32-DEVKITV1" deviceset="ESP32DEVKITV1" device=""/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="SUPPLY7" library="SparkFun-PowerSymbols" deviceset="VCC" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="U$11" library="SOSA" deviceset="NC" device=""/>
<part name="U$12" library="SOSA" deviceset="NC" device=""/>
<part name="U$13" library="SOSA" deviceset="NC" device=""/>
<part name="U$14" library="SOSA" deviceset="NC" device=""/>
<part name="U$15" library="SOSA" deviceset="NC" device=""/>
<part name="U$16" library="SOSA" deviceset="NC" device=""/>
<part name="U$18" library="SOSA" deviceset="NC" device=""/>
<part name="U$19" library="SOSA" deviceset="NC" device=""/>
<part name="U$20" library="SOSA" deviceset="NC" device=""/>
<part name="U$21" library="SOSA" deviceset="NC" device=""/>
<part name="U$23" library="SOSA" deviceset="NC" device=""/>
<part name="U$24" library="SOSA" deviceset="NC" device=""/>
<part name="U$25" library="SOSA" deviceset="NC" device=""/>
<part name="U$26" library="SOSA" deviceset="NC" device=""/>
<part name="U$27" library="SOSA" deviceset="NC" device=""/>
<part name="U$29" library="SOSA" deviceset="NC" device=""/>
<part name="GND11" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R3" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" value="1k"/>
<part name="R5" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R4" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" value="1k"/>
<part name="U6" library="esp32-environ-monitor-temp" deviceset="TVS_DIODE_ARRAY" device="SMD" value="TVS Diode"/>
<part name="GND13" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C4" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%" value="1.0uF"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%" value="1.0uF"/>
<part name="GND14" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND20" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY8" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="SUPPLY10" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="SUPPLY11" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" value="4k7"/>
<part name="U2" library="Weather Station GEN2" deviceset="CJMCU-811" device=""/>
<part name="U$1" library="SOSA" deviceset="NC" device=""/>
<part name="U$2" library="SOSA" deviceset="NC" device=""/>
<part name="U$3" library="SOSA" deviceset="NC" device=""/>
<part name="GND9" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY12" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="U$4" library="SOSA" deviceset="NC" device=""/>
<part name="U$5" library="SOSA" deviceset="NC" device=""/>
<part name="R2" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="SUPPLY13" library="SparkFun-PowerSymbols" deviceset="VCCIO" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="10UF-POLAR" device="-0603-6.3V-20%(TANT)" value="10uF"/>
<part name="C2" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%-X7R" value="1.0uF"/>
<part name="C3" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%-X7R" value="1.0uF"/>
<part name="GND10" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND15" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND16" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="J3" library="esp32-environ-monitor-temp" deviceset="WEATHER_METER-RAIN" device=""/>
<part name="J4" library="esp32-environ-monitor-temp" deviceset="WEATHER_METER-WIND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="6.35" y="170.18" size="2.54" layer="94" ratio="10">ESP32 Microcontroller</text>
<wire x1="97.79" y1="175.26" x2="97.79" y2="95.25" width="0.1524" layer="95" style="longdash"/>
<wire x1="97.79" y1="95.25" x2="3.81" y2="95.25" width="0.1524" layer="95" style="longdash"/>
<wire x1="97.79" y1="95.25" x2="97.79" y2="74.93" width="0.1524" layer="95" style="longdash"/>
<wire x1="97.79" y1="74.93" x2="168.91" y2="74.93" width="0.1524" layer="95" style="longdash"/>
<wire x1="168.91" y1="74.93" x2="212.09" y2="74.93" width="0.1524" layer="95" style="longdash"/>
<wire x1="212.09" y1="74.93" x2="256.54" y2="74.93" width="0.1524" layer="95" style="longdash"/>
<wire x1="212.09" y1="74.93" x2="212.09" y2="24.13" width="0.1524" layer="95" style="longdash"/>
<wire x1="168.91" y1="74.93" x2="168.91" y2="24.13" width="0.1524" layer="95" style="longdash"/>
<wire x1="97.79" y1="74.93" x2="97.79" y2="3.81" width="0.1524" layer="95" style="longdash"/>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="U3" gate="G$1" x="204.47" y="140.97" smashed="yes" rot="MR0">
<attribute name="NAME" x="217.17" y="152.4" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="U4" gate="G$1" x="204.47" y="97.79" smashed="yes" rot="MR0">
<attribute name="NAME" x="214.63" y="109.22" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="U5" gate="G$1" x="132.08" y="95.25" smashed="yes" rot="MR0">
<attribute name="NAME" x="142.24" y="106.68" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="GND2" gate="1" x="227.33" y="128.27" smashed="yes">
<attribute name="VALUE" x="227.33" y="128.016" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="224.79" y="151.13" smashed="yes">
<attribute name="VALUE" x="224.79" y="153.924" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND1" gate="1" x="224.79" y="85.09" smashed="yes">
<attribute name="VALUE" x="224.79" y="84.836" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="U$6" gate="G$1" x="219.71" y="102.87" smashed="yes"/>
<instance part="U$7" gate="G$1" x="222.25" y="146.05" smashed="yes"/>
<instance part="SUPPLY2" gate="G$1" x="222.25" y="107.95" smashed="yes">
<attribute name="VALUE" x="222.25" y="110.744" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="149.86" y="105.41" smashed="yes">
<attribute name="VALUE" x="149.86" y="108.204" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND3" gate="1" x="149.86" y="85.09" smashed="yes">
<attribute name="VALUE" x="149.86" y="84.836" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="U7" gate="G$1" x="130.81" y="30.48" smashed="yes">
<attribute name="NAME" x="122.682" y="25.4" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="J1" gate="G$1" x="109.22" y="45.72" smashed="yes" rot="MR0">
<attribute name="NAME" x="111.76" y="51.562" size="1.4224" layer="95" rot="MR0"/>
</instance>
<instance part="GND4" gate="1" x="130.81" y="15.24" smashed="yes">
<attribute name="VALUE" x="130.81" y="14.986" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND5" gate="1" x="119.38" y="15.24" smashed="yes">
<attribute name="VALUE" x="119.38" y="14.986" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="135.89" y="60.96" smashed="yes">
<attribute name="VALUE" x="135.89" y="63.754" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="J2" gate="G$1" x="242.57" y="48.26" smashed="yes">
<attribute name="NAME" x="240.03" y="59.182" size="1.4224" layer="95"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="232.41" y="62.23" smashed="yes">
<attribute name="VALUE" x="232.41" y="65.024" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND6" gate="1" x="232.41" y="33.02" smashed="yes">
<attribute name="VALUE" x="232.41" y="32.766" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="U1" gate="G$1" x="48.26" y="124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="35.56" y="151.13" size="1.27" layer="95"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="27.94" y="153.67" smashed="yes">
<attribute name="VALUE" x="27.94" y="156.464" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="68.58" y="153.67" smashed="yes">
<attribute name="VALUE" x="68.58" y="156.464" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND7" gate="1" x="25.4" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="25.146" y="144.78" size="1.778" layer="96" rot="R270" align="top-center"/>
</instance>
<instance part="GND8" gate="1" x="71.12" y="144.78" smashed="yes" rot="R90">
<attribute name="VALUE" x="71.374" y="144.78" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="U$11" gate="G$1" x="30.48" y="111.76" smashed="yes"/>
<instance part="U$12" gate="G$1" x="30.48" y="116.84" smashed="yes"/>
<instance part="U$13" gate="G$1" x="30.48" y="119.38" smashed="yes"/>
<instance part="U$14" gate="G$1" x="66.04" y="114.3" smashed="yes"/>
<instance part="U$15" gate="G$1" x="66.04" y="116.84" smashed="yes"/>
<instance part="U$16" gate="G$1" x="66.04" y="111.76" smashed="yes"/>
<instance part="U$18" gate="G$1" x="30.48" y="134.62" smashed="yes"/>
<instance part="U$19" gate="G$1" x="30.48" y="132.08" smashed="yes"/>
<instance part="U$20" gate="G$1" x="30.48" y="127" smashed="yes"/>
<instance part="U$21" gate="G$1" x="30.48" y="124.46" smashed="yes"/>
<instance part="U$23" gate="G$1" x="66.04" y="127" smashed="yes"/>
<instance part="U$24" gate="G$1" x="66.04" y="132.08" smashed="yes"/>
<instance part="U$25" gate="G$1" x="66.04" y="129.54" smashed="yes"/>
<instance part="U$26" gate="G$1" x="66.04" y="134.62" smashed="yes"/>
<instance part="U$27" gate="G$1" x="66.04" y="137.16" smashed="yes"/>
<instance part="U$29" gate="G$1" x="66.04" y="139.7" smashed="yes"/>
<instance part="GND11" gate="1" x="29.21" y="45.72" smashed="yes">
<attribute name="VALUE" x="29.21" y="45.466" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND12" gate="1" x="30.48" y="8.89" smashed="yes" rot="MR0">
<attribute name="VALUE" x="30.48" y="8.636" size="1.778" layer="96" rot="MR0" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="66.04" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="64.516" y="27.94" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="67.564" y="27.94" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="59.69" y="77.47" smashed="yes" rot="R90">
<attribute name="NAME" x="58.166" y="74.93" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="58.166" y="80.264" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="68.58" y="77.47" smashed="yes" rot="R90">
<attribute name="NAME" x="69.85" y="74.93" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="69.85" y="80.264" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="U6" gate="U1" x="187.96" y="48.26" smashed="yes">
<attribute name="NAME" x="195.326" y="53.34" size="1.778" layer="95" rot="MR270"/>
</instance>
<instance part="GND13" gate="1" x="187.96" y="33.02" smashed="yes" rot="MR0">
<attribute name="VALUE" x="187.96" y="32.766" size="1.778" layer="96" rot="MR0" align="top-center"/>
</instance>
<instance part="C4" gate="G$1" x="63.5" y="15.24" smashed="yes">
<attribute name="NAME" x="65.024" y="18.161" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="65.024" y="13.081" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C5" gate="G$1" x="71.12" y="53.34" smashed="yes">
<attribute name="NAME" x="72.644" y="56.769" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="72.898" y="51.435" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND14" gate="1" x="71.12" y="45.72" smashed="yes" rot="MR0">
<attribute name="VALUE" x="71.12" y="45.466" size="1.778" layer="96" rot="MR0" align="top-center"/>
</instance>
<instance part="GND20" gate="1" x="63.5" y="8.89" smashed="yes" rot="MR0">
<attribute name="VALUE" x="63.5" y="8.636" size="1.778" layer="96" rot="MR0" align="top-center"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="59.69" y="85.09" smashed="yes">
<attribute name="VALUE" x="59.69" y="87.884" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="68.58" y="85.09" smashed="yes">
<attribute name="VALUE" x="68.58" y="87.884" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="187.96" y="60.96" smashed="yes">
<attribute name="VALUE" x="187.96" y="63.754" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="66.04" y="35.56" smashed="yes">
<attribute name="VALUE" x="66.04" y="38.354" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R1" gate="G$1" x="232.41" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="230.886" y="50.8" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="233.934" y="50.8" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="U2" gate="G$1" x="132.08" y="138.43" smashed="yes" rot="MR0">
<attribute name="NAME" x="140.97" y="151.13" size="1.6764" layer="95" rot="MR0"/>
</instance>
<instance part="U$1" gate="G$1" x="146.05" y="132.08" smashed="yes"/>
<instance part="U$2" gate="G$1" x="146.05" y="134.62" smashed="yes"/>
<instance part="U$3" gate="G$1" x="146.05" y="137.16" smashed="yes"/>
<instance part="GND9" gate="1" x="148.59" y="121.92" smashed="yes">
<attribute name="VALUE" x="148.59" y="121.666" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="148.59" y="154.94" smashed="yes">
<attribute name="VALUE" x="148.59" y="157.734" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U$4" gate="G$1" x="30.48" y="137.16" smashed="yes"/>
<instance part="U$5" gate="G$1" x="30.48" y="129.54" smashed="yes"/>
<instance part="R2" gate="G$1" x="160.02" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="158.496" y="144.78" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="158.496" y="150.114" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="SUPPLY13" gate="G$1" x="160.02" y="154.94" smashed="yes">
<attribute name="VALUE" x="160.02" y="157.734" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="C1" gate="G$1" x="140.97" y="52.07" smashed="yes">
<attribute name="NAME" x="141.986" y="52.705" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="141.986" y="47.879" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C2" gate="G$1" x="149.86" y="49.53" smashed="yes">
<attribute name="NAME" x="151.384" y="52.451" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="151.384" y="47.371" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C3" gate="G$1" x="158.75" y="49.53" smashed="yes">
<attribute name="NAME" x="160.274" y="52.451" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="160.274" y="47.371" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND10" gate="1" x="140.97" y="41.91" smashed="yes">
<attribute name="VALUE" x="140.97" y="41.656" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND15" gate="1" x="149.86" y="41.91" smashed="yes">
<attribute name="VALUE" x="149.86" y="41.656" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND16" gate="1" x="158.75" y="41.91" smashed="yes">
<attribute name="VALUE" x="158.75" y="41.656" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="J3" gate="G$1" x="45.72" y="21.59" smashed="yes">
<attribute name="NAME" x="35.56" y="26.924" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="J4" gate="G$1" x="44.45" y="68.58" smashed="yes">
<attribute name="NAME" x="34.29" y="76.454" size="1.778" layer="95" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="222.25" y1="138.43" x2="227.33" y2="138.43" width="0.1524" layer="91"/>
<wire x1="227.33" y1="138.43" x2="227.33" y2="130.81" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="219.71" y1="97.79" x2="224.79" y2="97.79" width="0.1524" layer="91"/>
<wire x1="224.79" y1="97.79" x2="224.79" y2="87.63" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="149.86" y1="97.79" x2="147.32" y2="97.79" width="0.1524" layer="91"/>
<wire x1="149.86" y1="97.79" x2="149.86" y2="87.63" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="130.81" y1="20.32" x2="130.81" y2="17.78" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="116.84" y1="43.18" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
<wire x1="119.38" y1="43.18" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="234.95" y1="40.64" x2="232.41" y2="40.64" width="0.1524" layer="91"/>
<wire x1="232.41" y1="40.64" x2="232.41" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="30.48" y1="144.78" x2="27.94" y2="144.78" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="29.21" y1="48.26" x2="29.21" y2="60.96" width="0.1524" layer="91"/>
<wire x1="29.21" y1="60.96" x2="29.21" y2="71.12" width="0.1524" layer="91"/>
<wire x1="31.75" y1="71.12" x2="29.21" y2="71.12" width="0.1524" layer="91"/>
<wire x1="31.75" y1="60.96" x2="29.21" y2="60.96" width="0.1524" layer="91"/>
<junction x="29.21" y="60.96"/>
<pinref part="J4" gate="G$1" pin="ANN-1"/>
<pinref part="J4" gate="G$1" pin="DIR-1"/>
</segment>
<segment>
<wire x1="33.02" y1="21.59" x2="30.48" y2="21.59" width="0.1524" layer="91"/>
<wire x1="30.48" y1="21.59" x2="30.48" y2="11.43" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="J3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="VSS"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="187.96" y1="35.56" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="71.12" y1="50.8" x2="71.12" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="63.5" y1="11.43" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="146.05" y1="129.54" x2="148.59" y2="129.54" width="0.1524" layer="91"/>
<wire x1="148.59" y1="129.54" x2="148.59" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="66.04" y1="144.78" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="-"/>
<wire x1="140.97" y1="46.99" x2="140.97" y2="44.45" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="149.86" y1="46.99" x2="149.86" y2="44.45" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="158.75" y1="46.99" x2="158.75" y2="44.45" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
</net>
<net name="VCCIO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="222.25" y1="135.89" x2="224.79" y2="135.89" width="0.1524" layer="91"/>
<wire x1="224.79" y1="135.89" x2="224.79" y2="151.13" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="3V3"/>
<wire x1="219.71" y1="100.33" x2="222.25" y2="100.33" width="0.1524" layer="91"/>
<junction x="222.25" y="100.33"/>
<wire x1="222.25" y1="100.33" x2="222.25" y2="107.95" width="0.1524" layer="91"/>
<wire x1="222.25" y1="92.71" x2="222.25" y2="100.33" width="0.1524" layer="91"/>
<wire x1="219.71" y1="92.71" x2="222.25" y2="92.71" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="EN"/>
<pinref part="SUPPLY2" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<wire x1="149.86" y1="100.33" x2="149.86" y2="105.41" width="0.1524" layer="91"/>
<wire x1="149.86" y1="100.33" x2="147.32" y2="100.33" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY3" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="VCC"/>
<wire x1="234.95" y1="55.88" x2="232.41" y2="55.88" width="0.1524" layer="91"/>
<wire x1="232.41" y1="55.88" x2="232.41" y2="62.23" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="VCCIO"/>
<pinref part="R1" gate="G$1" pin="2"/>
<junction x="232.41" y="55.88"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="3V3"/>
<wire x1="30.48" y1="147.32" x2="27.94" y2="147.32" width="0.1524" layer="91"/>
<wire x1="27.94" y1="147.32" x2="27.94" y2="153.67" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<wire x1="59.69" y1="85.09" x2="59.69" y2="82.55" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="68.58" y1="85.09" x2="68.58" y2="82.55" width="0.1524" layer="91"/>
<pinref part="SUPPLY9" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="VCC"/>
<wire x1="187.96" y1="60.96" x2="187.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="66.04" y1="35.56" x2="66.04" y2="33.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="146.05" y1="147.32" x2="148.59" y2="147.32" width="0.1524" layer="91"/>
<wire x1="148.59" y1="147.32" x2="148.59" y2="154.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="G$1" pin="VCCIO"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="160.02" y1="152.4" x2="160.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="G$1" pin="VCCIO"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SCL"/>
<wire x1="222.25" y1="140.97" x2="227.33" y2="140.97" width="0.1524" layer="91"/>
<label x="227.33" y="140.97" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SCL"/>
<wire x1="147.32" y1="95.25" x2="152.4" y2="95.25" width="0.1524" layer="91"/>
<label x="152.4" y="95.25" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO22"/>
<wire x1="30.48" y1="114.3" x2="27.94" y2="114.3" width="0.1524" layer="91"/>
<label x="27.94" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="146.05" y1="142.24" x2="148.59" y2="142.24" width="0.1524" layer="91"/>
<label x="148.59" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SDA"/>
<wire x1="222.25" y1="143.51" x2="227.33" y2="143.51" width="0.1524" layer="91"/>
<label x="227.33" y="143.51" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SDA"/>
<wire x1="147.32" y1="92.71" x2="152.4" y2="92.71" width="0.1524" layer="91"/>
<label x="152.4" y="92.71" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO21"/>
<wire x1="30.48" y1="121.92" x2="27.94" y2="121.92" width="0.1524" layer="91"/>
<label x="27.94" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="146.05" y1="144.78" x2="148.59" y2="144.78" width="0.1524" layer="91"/>
<label x="148.59" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SRADOUT" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT"/>
<wire x1="219.71" y1="95.25" x2="227.33" y2="95.25" width="0.1524" layer="91"/>
<label x="227.33" y="95.25" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO2"/>
<wire x1="30.48" y1="139.7" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<label x="27.94" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="POW"/>
<pinref part="U7" gate="G$1" pin="VIN"/>
<wire x1="116.84" y1="48.26" x2="125.73" y2="48.26" width="0.1524" layer="91"/>
<wire x1="125.73" y1="48.26" x2="125.73" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="VOUT"/>
<wire x1="135.89" y1="43.18" x2="135.89" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="G$1" pin="VCC"/>
<wire x1="135.89" y1="58.42" x2="135.89" y2="60.96" width="0.1524" layer="91"/>
<wire x1="135.89" y1="58.42" x2="140.97" y2="58.42" width="0.1524" layer="91"/>
<junction x="135.89" y="58.42"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="140.97" y1="58.42" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<wire x1="149.86" y1="58.42" x2="158.75" y2="58.42" width="0.1524" layer="91"/>
<wire x1="140.97" y1="54.61" x2="140.97" y2="58.42" width="0.1524" layer="91"/>
<junction x="140.97" y="58.42"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="149.86" y1="54.61" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<junction x="149.86" y="58.42"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="158.75" y1="58.42" x2="158.75" y2="54.61" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="66.04" y1="147.32" x2="68.58" y2="147.32" width="0.1524" layer="91"/>
<wire x1="68.58" y1="147.32" x2="68.58" y2="153.67" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="WIND_DIR" class="0">
<segment>
<label x="71.12" y="71.12" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="59.69" y1="71.12" x2="59.69" y2="72.39" width="0.1524" layer="91"/>
<wire x1="71.12" y1="71.12" x2="59.69" y2="71.12" width="0.1524" layer="91"/>
<wire x1="59.69" y1="71.12" x2="57.15" y2="71.12" width="0.1524" layer="91"/>
<junction x="59.69" y="71.12"/>
<pinref part="J4" gate="G$1" pin="DIR2"/>
</segment>
<segment>
<wire x1="185.42" y1="58.42" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U6" gate="U1" pin="IO4"/>
<wire x1="185.42" y1="55.88" x2="185.42" y2="58.42" width="0.1524" layer="91"/>
<label x="182.88" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO35"/>
<wire x1="66.04" y1="121.92" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<label x="68.58" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAINFALL" class="0">
<segment>
<label x="68.58" y="21.59" size="1.27" layer="95" xref="yes"/>
<wire x1="58.42" y1="21.59" x2="63.5" y2="21.59" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="63.5" y1="21.59" x2="66.04" y2="21.59" width="0.1524" layer="91"/>
<wire x1="66.04" y1="21.59" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<junction x="66.04" y="21.59"/>
<wire x1="68.58" y1="21.59" x2="66.04" y2="21.59" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="63.5" y1="20.32" x2="63.5" y2="21.59" width="0.1524" layer="91"/>
<junction x="63.5" y="21.59"/>
<pinref part="J3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="IO2"/>
<wire x1="190.5" y1="40.64" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="190.5" y1="38.1" x2="193.04" y2="38.1" width="0.1524" layer="91"/>
<label x="193.04" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO32"/>
<wire x1="66.04" y1="124.46" x2="68.58" y2="124.46" width="0.1524" layer="91"/>
<label x="68.58" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="WIND_SPEED" class="0">
<segment>
<wire x1="57.15" y1="60.96" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="68.58" y1="72.39" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<junction x="68.58" y="60.96"/>
<wire x1="73.66" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<label x="73.66" y="60.96" size="1.27" layer="95" xref="yes"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="71.12" y1="60.96" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<wire x1="71.12" y1="58.42" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="71.12" y="60.96"/>
<pinref part="J4" gate="G$1" pin="ANN-2"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="IO3"/>
<wire x1="190.5" y1="55.88" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<wire x1="190.5" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<label x="193.04" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO34"/>
<wire x1="66.04" y1="119.38" x2="68.58" y2="119.38" width="0.1524" layer="91"/>
<label x="68.58" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TEMP_BUS" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="DAT"/>
<wire x1="234.95" y1="45.72" x2="232.41" y2="45.72" width="0.1524" layer="91"/>
<label x="227.33" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="232.41" y1="45.72" x2="227.33" y2="45.72" width="0.1524" layer="91"/>
<junction x="232.41" y="45.72"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO13"/>
<wire x1="66.04" y1="142.24" x2="68.58" y2="142.24" width="0.1524" layer="91"/>
<label x="68.58" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="!WAKE" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="WAK"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="146.05" y1="139.7" x2="160.02" y2="139.7" width="0.1524" layer="91"/>
<wire x1="160.02" y1="139.7" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<wire x1="160.02" y1="139.7" x2="163.83" y2="139.7" width="0.1524" layer="91"/>
<junction x="160.02" y="139.7"/>
<label x="163.83" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="IO15"/>
<wire x1="30.48" y1="142.24" x2="27.94" y2="142.24" width="0.1524" layer="91"/>
<label x="27.94" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
