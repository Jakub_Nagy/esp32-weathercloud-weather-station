void setup()
{
  Serial.begin(9600);
}

void loop()
{
  float voltage = (analogRead(34) * (3.3 / 4095));
  float UVIndex = (voltage - 1)* 7.5;
  if(voltage > 2.46) UVIndex = 11;
  if(voltage < 1) UVIndex = 0;
  Serial.print("  Voltage: ");
  Serial.print(voltage);
  Serial.print("        UV index: ");
  Serial.println(UVIndex);
  delay(1000);
}
