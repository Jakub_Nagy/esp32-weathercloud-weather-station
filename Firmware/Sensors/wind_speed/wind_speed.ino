boolean state = false;
int clicked;
unsigned long lastMillis = 0;
 
void setup(){
    Serial.begin(115200); 
    pinMode(26, INPUT);
}
 
void loop(){
    Serial.println("Starting wind speed measurment period.");
    lastMillis = xTaskGetTickCount();
    while(xTaskGetTickCount() - lastMillis < 10000){
        if(digitalRead(26) == HIGH) if(state == false){
            delay(50);
            clicked++;
            state = true;
        }
        if(digitalRead(26) == LOW) if(state == true) state = false;
    }
    Serial.print("Clicked: ");
    Serial.println(clicked);
    Serial.print("RPS: ");
    Serial.println(clicked / 10);
    Serial.print("Wind speed: ");
    Serial.print(clicked * 0.0333);
    Serial.println("m/s");
    Serial.println(" ");
    clicked = 0;
    delay(5000);
}
