//temperature
#include <OneWire.h>
#include <DallasTemperature.h>
OneWire oneWire(4); 
DallasTemperature sensors(&oneWire);
DeviceAddress groundThermometer = { 0x28,  0xD4,  0xB0,  0x26,  0x0,  0x0,  0x80,  0xBC };
DeviceAddress airThermometer = { 0x28,  0xF4,  0xBC,  0x26,  0x0,  0x0,  0x80,  0x2B };
//-------------------------------------------------------------------------------------
//humidity and pressure
#include <BME280I2C.h>
#include <Wire.h>
#include "DHT.h"
DHT dht(18, DHT21);
BME280I2C bme;
//-------------------------------------------------------------------------------------
//solar radiation
#include <BH1750.h>
BH1750 lightMeter;
//-------------------------------------------------------------------------------------
//wind speed
boolean state = false;
int clicked, wspd;
unsigned long lastMillis = 0;
//-------------------------------------------------------------------------------------
//rainfall
#include <Wire.h>
int input, rainfall;
//-------------------------------------------------------------------------------------
//RTC
#include <Wire.h>
#include "RTClib.h"
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
//-------------------------------------------------------------------------------------
void setup(){
    Serial.begin(115200);
    //temperature
    sensors.begin();
    sensors.getAddress(airThermometer, 1); 
    sensors.getAddress(groundThermometer, 0); 
    sensors.setResolution(airThermometer, 15);
    sensors.setResolution(groundThermometer, 15);
    //-------------------------------------------------------------------------------------
    //humidity and pressure
    Wire.begin();
    bme.begin();
    //-------------------------------------------------------------------------------------
    //solar radiation
    lightMeter.begin();
    //-------------------------------------------------------------------------------------
    //wind speed
    pinMode(26, INPUT);
    //-------------------------------------------------------------------------------------
    //RTC
    rtc.begin();
}
//----------------------------------------------------------------------------------------------------------
void loop(){
    //temperature
    sensors.requestTemperatures();
    delay(10);
    float tempair =  sensors.getTempC(airThermometer);
    float tempground = sensors.getTempC(groundThermometer);
    int temp = int(tempair*10);
    int tempin = int(tempground*10);
    //-------------------------------------------------------------------------------------
    //humidity and pressure
    float tempe(0), hum(0), pres(0);
    bme.read(pres, tempe, hum, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
    int bar = int(pres/10);
    //-------------------------------------------------------------------------------------
    //heat index
    float hic = dht.computeHeatIndex(tempair, hum, false);
    int heat = int(hic*10);
    //-------------------------------------------------------------------------------------
    //dew point
    double gamma = log(hum / 100) + ((17.62 * tempair) / (243.5 + tempair));
    double dp = (243.5 * gamma / (17.62 - gamma));
    int dew = int(dp*10);
    //-------------------------------------------------------------------------------------
    //solar radiation
    float lux = lightMeter.readLightLevel();
    int solarrad = lux*0.079;
    //-------------------------------------------------------------------------------------
    //UV radiation
    float voltage = (analogRead(34) * (3.3 / 4095));
    float UVIndex = (voltage - 1)* 7.5;
    if(voltage > 2.46) UVIndex = 11;
    if(voltage < 1) UVIndex = 0;
    int uvi = int(UVIndex);
    //-------------------------------------------------------------------------------------
    //wind speed
    Serial.println("Starting wind speed measurment period.");
    lastMillis = xTaskGetTickCount();
    while(xTaskGetTickCount() - lastMillis < 10000){
        if(digitalRead(26) == HIGH) if(state == false){
            delay(50);
            clicked++;
            state = true;
        }
        if(digitalRead(26) == LOW) if(state == true) state = false;
    }
    float mps = clicked * 0.0333;
    float kph = mps * 3.6;
    //-------------------------------------------------------------------------------------
    //rainfall
    Wire.requestFrom(105, 4);
    while (Wire.available()){ 
    input = Wire.read();
        if(input == 1){
        rainfall++;
        delay(100);
        }
    }
    //-------------------------------------------------------------------------------------
    //wind chill
    float chill = (13.12 + (0.62 * tempair) - (11.37 * (pow(kph, 0.16))) + (0.39 * tempair * (pow(kph, 0.16))));
    int wchill = int(chill*10);
    if(clicked < 2)wchill = temp;
    if(tempair > 30)wchill = temp;
    //-------------------------------------------------------------------------------------
    //RTC
    DateTime now = rtc.now();
    //-------------------------------------------------------------------------------------
    //print
    Serial.print("Air temperature: ");
    Serial.print(tempair);
    Serial.println("°C");
    Serial.print("Ground temperature: ");
    Serial.print(tempground);
    Serial.println("°C");
    Serial.print("Heat index: ");
    Serial.print(hic);
    Serial.println("°C");
    Serial.print("Dew point: ");
    Serial.print(dp);
    Serial.println("°C");
    Serial.print("Wind chill: ");
    Serial.print(chill);
    Serial.println("°C");
    Serial.print("Humidity: ");
    Serial.print(hum);
    Serial.println("%");
    Serial.print("Pressure: ");
    Serial.print(pres/100);
    Serial.println("hPa");
    Serial.print("Solar radiation: ");
    Serial.print(solarrad);
    Serial.println(" W/m2");
    Serial.print("UV index: ");
    Serial.println(UVIndex);
    Serial.print("Rainfall: ");
    Serial.print(rainfall);
    Serial.println(" mm/m2");
    Serial.print("Wind speed: ");
    Serial.print(mps);
    Serial.println( "m/s");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.println(now.second(), DEC);
    Serial.println();
    //end
    Serial.println(" ");
    delay(5000);
    clicked = 0;
}
