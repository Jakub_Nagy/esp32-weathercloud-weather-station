#include <DS3231.h>
DS3231  rtc(21, 22);

void setup()
{
  Serial.begin(9600);
  rtc.begin();
}

void loop()
{
  Serial.print("time: ");
  Serial.println(rtc.getTimeStr());
  
  delay (1000);
}
