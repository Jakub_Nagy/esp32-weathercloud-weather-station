#include <BME280I2C.h>
#include <Wire.h>
BME280I2C bme;

void setup(){
    Serial.begin(9600);
    Wire.begin();
    bme.begin();
}
void loop(){  
     float temp(NAN), hum(NAN), pres(NAN);
     bme.read(pres, temp, hum, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
     float alt = ((pow((1013.25 / (pres/100)),(1/5.257)) - 1) * (temp + 273.15)) / 0.0065;
     Serial.print("Temperature: ");
     Serial.print(temp);
     Serial.print("°C");
     Serial.print("    Humidity: ");
     Serial.print(hum);
     Serial.print("%");
     Serial.print("    Pressure: ");
     Serial.print(pres/100);
     Serial.print("hPa");
     Serial.print("    Altitude: ");
     Serial.print(alt);
     Serial.println("m");
     delay(1000);
}
