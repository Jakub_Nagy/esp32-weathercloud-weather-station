//=====================================================================================
//                                                                                   //
//                 ESP32 Weathercloud Weather Station Firmware                       //
//                                                                                   //
//                       Developed by Jakub Nagy, 2020                               //
//                       Program under the MIT license                               //
//                                                                                   //
//=====================================================================================
//User setup (change these variables according to your needs)
const char* ssid     = "UPC2595472";      //Type in your Wi-Fi network SSID (name)
const char* password = "Wa8tecakz2pa";    //Type in your Wi-Fi network password
const char* Weathercloud_ID  = "04c10c3eaf134380";                    //Copy and paste your Weathercloud ID here
const char* Weathercloud_KEY = "e0d8a4f79be48b6992c7e643483b2424";    //Copy and paste your Weathercloud KEY here

const long  gmtOffset_sec = 3600;        //Put here your local time offset in seconds (example GMT+1 = +1 * 3600s = 3600s)
const int   daylightOffset_sec = 3600;   //Do you have daylight saving time? Yes = 3600, No = 0
//=====================================================================================
//Wi-Fi and NTP client setup
#include <WiFi.h>
WiFiClient client;

const int httpPort = 80;
const char* Weathercloud = "http://api.weathercloud.net";
const char* streamId   = "....................";
const char* privateKey = "....................";

void wifi_connect()
{
    int connectTries = 0;
       
    WiFi.mode(WIFI_OFF);
    delay(200);
    WiFi.mode(WIFI_STA);
    
    Serial.print("Connecting to "); Serial.println(ssid);
    WiFi.begin(ssid, password);
    while(WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");

        if(connectTries > 20) wifi_connect();
        connectTries++;
    } 
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println(" ");
}
//=====================================================================================
//NTP client setup
#include "time.h"
struct tm timeinfo;

const char* ntpServer = "pool.ntp.org";
RTC_DATA_ATTR int lastminute, lasthour, lastday;

void get_local_time()
{
  Serial.println("Pulling local time from NTP server.");
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time.\n");
    return;
  }
  Serial.print("Time pulled from NTP server sucessfuly. Time: ");
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S\n");
}
//=====================================================================================
//Deep sleep
void sleep(int sleeptime){
    Serial.println("Weather station going to sleep...");
    esp_sleep_enable_timer_wakeup(sleeptime * 1000000);
    delay(1000);
    Serial.flush(); 
    esp_deep_sleep_start();
}
//=====================================================================================
//testing data
int temp = 123;
int tempin = 256;
int chill = 51;
int dew = 23;
int heat = 113;
int hum = 50;
int wspd = 51;
int wdir = 236;
int bar = 10024;
RTC_DATA_ATTR int rainfall;
RTC_DATA_ATTR int rainrate;
int uvi = 42;
int solarrad = 494;
//=====================================================================================
//rain
void read_rain()
{
    rainfall++;
    rainrate++;
    
    if(timeinfo.tm_min != lastminute)
    {  
      Serial.print("Minute has changed. Last minute: "); Serial.println(lastminute);
    }
    if(timeinfo.tm_hour != lasthour)
    {
      Serial.print("Hour has changed. Last hour: "); Serial.println(lasthour);
      rainrate = 0;
    }
    if(timeinfo.tm_mday != lastday)
    {
      Serial.print("Day has changed. Last day: "); Serial.println(lastday);
      rainfall = 0;
    }
    Serial.println(" ");
  
    lastminute = timeinfo.tm_min;
    lasthour = timeinfo.tm_hour;
    lastday = timeinfo.tm_mday;

    Serial.print("Rainfall: "); Serial.println(rainfall);
    Serial.print("Rainrate: "); Serial.println(rainrate);
    Serial.println("");
}
//=====================================================================================
//Weathercloud push function
void push_to_weathercloud()
{
    Serial.println("Initializing data push to Weathercloud.");

    if (!client.connect(Weathercloud, httpPort)) {
        Serial.println("Connecting to Weatherloud failed.\n");
        return;
    }
    
    client.print("GET /set");
    client.print("/wid/"); client.print(Weathercloud_ID);
    client.print("/key/"); client.print(Weathercloud_KEY);
    
    client.print("/temp/"); client.print(temp);
    client.print("/tempin/"); client.print(tempin);
    client.print("/chill/"); client.print(chill);
    client.print("/dew/"); client.print(dew);
    client.print("/heat/"); client.print(heat);
    client.print("/hum/"); client.print(hum);
    client.print("/wspd/"); client.print(wspd);
    client.print("/wdir/"); client.print(wdir);
    client.print("/bar/"); client.print(bar);
    client.print("/rain/"); client.print(rainfall);
    client.print("/rainrate/"); client.print(rainrate);
    client.print("/uvi/"); client.print(uvi);
    client.print("/solarrad/"); client.print(solarrad);

    client.println("/ HTTP/1.1");
    client.println("Host: api.weathercloud.net");
    client.println("Connection: close");
    client.println();
    Serial.println("Data pushed to Weathercloud sucessfuly.\n");;
}
//=====================================================================================
//setup
void setup()
{
    Serial.begin(115200);
    Serial.println("\nWeather station powered on.\n");
    wifi_connect();
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
}
//=====================================================================================
//loop
void loop()
{
    get_local_time();

    read_rain();
    
    push_to_weathercloud();

    sleep(300);    
}
//=====================================END OF CODE=====================================