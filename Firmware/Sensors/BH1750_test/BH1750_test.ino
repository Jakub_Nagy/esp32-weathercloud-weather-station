#include <Wire.h>
#include <BH1750.h>
BH1750 lightMeter;


void setup(){
  Serial.begin(9600);
  Wire.begin();
  lightMeter.begin();
}

void loop() {
  float lux = lightMeter.readLightLevel();
  Serial.print("Light: ");
  Serial.print(lux*0.0079);
  Serial.println(" W/m2");
  delay(1000);

}
