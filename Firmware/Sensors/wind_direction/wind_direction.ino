int vpr, wdir;

void setup() {
 Serial.begin(9600); 
}

void loop() {
  int ar = analogRead(35);
  if(ar > 2155 && ar < 2207)   wdir = 0;
  if (ar > 502  && ar < 554) wdir = 22.5;
  if (ar > 649  && ar < 699) wdir = 45;
  //if (ar >  && ar < ) wdir = 67.5;
  //if (ar > 0 && ar < 0) wdir = 90;
  if (ar > 49 && ar < 90) wdir = 112.5;
  if (ar > 89 && ar < 111) wdir = 135;
  if (ar > 0 && ar < 11) wdir = 157.5;
  if (ar > 242 && ar < 280) wdir = 180;
  if (ar > 159 && ar < 211) wdir = 202.5;
  if (ar > 1239 && ar < 1301) wdir = 225;
  if (ar > 1129 && ar < 1161) wdir = 247.5;
  if (ar > 4069 && ar < 5001) wdir = 270;
  if (ar > 2470 && ar < 2516) wdir = 292.5;
  if (ar > 3101 && ar < 3321) wdir = 315;
  if (ar > 1629 && ar < 1656) wdir = 337.5;
  if(ar == 0){
       delay(200);
       ar = analogRead(35);
       if(ar == 0) wdir = 90;
       else wdir = 157.5;
  }
  
  Serial.print("Analog reading: ");
  Serial.println(ar);
  Serial.print("wind direction: ");
  Serial.print(wdir);
  Serial.println("°");
  Serial.println(" ");
  delay(1000);
}
