//
//           ESP32 Weathercloud Weather Station Weathercloud API test
//
//                       Developed by Jakub Nagy in 2020
//                        Program under the MIT license
//=====================================================================================
//Wi-Fi and NTP client setup
#include <WiFi.h>
#include "time.h"
WiFiClient client;

const char* ssid     = "UPC2595472";
const char* password = "Wa8tecakz2pa";
const char* Weathercloud_ID  = "ce557d3b0b7999e5";
const char* Weathercloud_KEY = "6253bddd3a9039db97add42b23af9ea0";

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;
struct tm timeinfo;

const int httpPort = 80;
const char* Weathercloud = "http://api.weathercloud.net";
const char* streamId   = "....................";
const char* privateKey = "....................";

void wifi_connect()
{
    delay(100);
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println(" ");
}

void get_local_time()
{
  Serial.println("Pulling local time from NTP server.");
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time.\n");
    return;
  }
  Serial.print("Time pulled from NTP server sucessfuly. Time: ");
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S\n");
}
//=====================================================================================
//testing data
int temp = 123;
int tempin = 256;
int chill = 51;
int dew = 23;
int heat = 113;
int hum = 50;
int wspd = 51;
int wdir = 236;
int bar = 10024;
int rain = 0;
int rainrate = 0;
int uvi = 04;
//=====================================================================================
//Weathercloud push function
void push_to_weathercloud()
{
    Serial.println("Initializing data push to Weathercloud.");

    if (!client.connect(Weathercloud, httpPort)) {
        Serial.println("Connecting to Weatherloud failed.\n");
        return;
    }
    
    client.print("GET /set");
    client.print("/wid/"); client.print(Weathercloud_ID);
    client.print("/key/"); client.print(Weathercloud_KEY);
    
    client.print("/temp/"); client.print(temp);
    client.print("/tempin/"); client.print(tempin);
    client.print("/chill/"); client.print(chill);
    client.print("/dew/"); client.print(dew);
    client.print("/heat/"); client.print(heat);
    client.print("/hum/"); client.print(hum);
    client.print("/wspd/"); client.print(wspd);
    client.print("/wdir/"); client.print(wdir);
    client.print("/bar/"); client.print(bar);
    client.print("/rain/"); client.print(rain);
    client.print("/rainrate/"); client.print(rainrate);
    client.print("/uvi/"); client.print(uvi);
    client.print("/rain/"); client.print(rain);
    client.print("/rainrate/"); client.print(rainrate);

    client.println("/ HTTP/1.1");
    client.println("Host: api.weathercloud.net");
    client.println("Connection: close");
    client.println();
    Serial.println("Data pushed to Weathercloud sucessfuly.\n");;
}
//=====================================================================================
//setup
void setup()
{
    Serial.begin(115200);
    Serial.println("\nWeather station powered on.\n");
    wifi_connect();
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
}
//=====================================================================================
//loop
void loop()
{
    get_local_time();
    push_to_weathercloud();
    delay(20000);
}
//=====================================END OF CODE=====================================