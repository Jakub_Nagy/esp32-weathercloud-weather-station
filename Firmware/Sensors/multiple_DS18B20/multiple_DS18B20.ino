#include <OneWire.h>
#include <DallasTemperature.h>
OneWire oneWire(4); 
DallasTemperature sensors(&oneWire);
DeviceAddress insideThermometer = { 0x28,  0xD4,  0xB0,  0x26,  0x0,  0x0,  0x80,  0xBC };
DeviceAddress outsideThermometer = { 0x28,  0xF4,  0xBC,  0x26,  0x0,  0x0,  0x80,  0x2B };

void setup() {
  Serial.begin(115200);
  sensors.begin();
  sensors.getAddress(insideThermometer, 0); 
  sensors.getAddress(outsideThermometer, 1); 
  sensors.setResolution(insideThermometer, 10);
  sensors.setResolution(outsideThermometer, 10);
}

void loop() {
  sensors.requestTemperatures();
  delay(10);
  float tempin =  sensors.getTempC(insideThermometer);
  float tempout = sensors.getTempC(outsideThermometer);
  Serial.println(tempin);
  Serial.println(tempout);
  delay(2500);
}
