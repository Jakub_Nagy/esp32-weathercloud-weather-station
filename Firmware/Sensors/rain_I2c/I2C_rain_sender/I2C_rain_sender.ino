//
//                 ESP32 Weathercloud Weather Station Firmware
//
//                       Developed by Jakub Nagy, 2020
//                       Program under the MIT license
//
//=====================================================================================
//I2C port setup
#include <Wire.h>
int state;
//=====================================================================================
//setup
void setup() {
  pinMode(4, INPUT);
  pinMode(13, OUTPUT);
  Wire.begin(105);                
  Wire.onRequest(requestEvent);
}
//=====================================================================================
//loop
void loop() {
    if(digitalRead(4) == HIGH){
      state = 1;
      digitalWrite(13, HIGH);
      delay(60);
      digitalWrite(13, LOW);
    }
}
//=====================================================================================
//I2C request event
void requestEvent() {   
   Wire.write(state);
   state = 0; 
}
//=====================================END OF CODE====================================