#include <Wire.h>
int rpd, rph, a, b, input;

#include <DS3231.h>
DS3231  rtc(SDA, SCL);
Time  t;

void setup() {
  Serial.begin(9600);
  pinMode(7, OUTPUT);
  Wire.begin();
  rtc.begin();
}

void loop() {
  Wire.requestFrom(8, 4);
  while (Wire.available()){ 
  input = Wire.read();
      if(input == 1){
      a++;
      b++;
      delay(100);
      }
  }
  rpd = b * 3;
  rph = a * 3;
  t = rtc.getTime();   
  if(t.min  == 0){
    rph = 0;
    a = 0;
  }
  if(t.hour  == 0){
    rpd = 0;
    b = 0;
  }
    Serial.print("rainfall:");
  if(rpd < 10){
    Serial.print("0");
    Serial.println(rpd);
  }
  else{
    Serial.println(rpd);
  }
  Serial.print("rainrate:");
  if(rph < 10){
    Serial.print("0");
    Serial.println(rph);
  }
  else{
    Serial.println(rph);
  }
  delay(1000);  
}
